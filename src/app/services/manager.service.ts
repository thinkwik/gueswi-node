import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IMember} from '../models/member';


@Injectable()
export class ManagerService {
    constructor(private _http: Http) {

    }

    getAll(): Observable<IMember[]> {
        let url = `${GlobalService.apiPath}/staff/managers/getAll`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IMember[]>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    handleError(error: any): any {
        return Observable.throw(error.json());
    }

}


@Injectable()
export class ManagerListResolver implements Resolve<IMember[]> {

    constructor(private _http: Http) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IMember[] | Observable<IMember[]> | Promise<IMember[]> {
        let url = `${GlobalService.apiPath}/staff/managers/getAll`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .toPromise()
            .then(res => res.json() as IMember[])
            .catch(err => Promise.reject(err))
    }
}

