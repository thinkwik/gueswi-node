import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {IOffice} from '../models/office';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class OfficeService {
    constructor(private _http: Http) {

    }

    getAll(filters?: any): Observable<IOffice[]> {
        let url = `${GlobalService.apiPath}/office/getAll`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, filters, {headers: headers})
            .map((res: Response) => <IOffice[]>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    create(payload: any): Observable<IOffice> {
        let url = `${GlobalService.apiPath}/office/create`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => <IOffice>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    update(payload: any, id: string): Observable<IOffice> {
        let url = `${GlobalService.apiPath}/office/update/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.put(url, payload, {headers: headers})
            .map((res: Response) => <IOffice>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    delete(id: string): Observable<IOffice> {
        let url = `${GlobalService.apiPath}/office/delete/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.delete(url, {headers: headers})
            .map((res: Response) => <IOffice>res.json())
            .catch((err: Error) => this.handleError(err))
    }



    addOrRemoveStaffFromOffice(payload: any, method: string): Observable<any> {
        let url = '';
        switch (method) {
            case 'add':
                url = `${GlobalService.apiPath}/office/add/staff`;
                break;
            case 'remove':
                url = `${GlobalService.apiPath}/office/delete/staff`;
                break;
        }
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: Error) => this.handleError(err))
    }

    getOfficesByCompany(field: string, query: any): Observable<IOffice[]> {
        let url = '';
        switch (field) {
            case 'id':
                url = `${GlobalService.apiPath}/office/getDataByCompanyId/${query}`;
                break;
            case 'name':
                url = `${GlobalService.apiPath}/office/getDataByCompanyName/${query.company}/${query.name}`;
                break;
        }
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IOffice[]>res.json())
            .catch((err: Error) => this.handleError(err))
    }


    handleError(error: any): any {
        return Observable.throw(error.json());
    }
}


@Injectable()
export class OfficeResolver implements Resolve<IOffice> {

    constructor(private _http: Http) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IOffice | Observable<IOffice> | Promise<IOffice> {
        let id = route.params.id;
        let url = `${GlobalService.apiPath}/office/get/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .toPromise()
            .then(res => res.json() as IOffice)
            .catch(err => Promise.reject(err))
    }
}
