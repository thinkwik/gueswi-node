import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {IUser} from '../models/user';
import {GlobalService} from './global.service';
import {ActivatedRouteSnapshot, CanActivate, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {IMember} from '../models/member';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class StaffService {

    public user: Subject<IMember> = new Subject();

    constructor(private _http: Http) {

    }

    login(payload: any): Observable<IUser> {
        let url = `${GlobalService.apiPath}/staff/login`;
        return this._http.post(url, payload)
            .map((res: Response) => <IUser>res.json())
            .do(res => {
                localStorage.setItem('token', res['access_token']);
                if (res['data'].company) localStorage.setItem('_bid', res['data'].company._id);
            })
            .catch((err: Error) => this.handleError(err));
    }

    get(): Observable<IMember> {
        let url = `${GlobalService.apiPath}/staff/me`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IMember>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    getById(id: string): Observable<IUser> {
        let url = `${GlobalService.apiPath}/staff/get/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IUser>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    create(payload: any): Observable<IUser> {
        let url = `${GlobalService.apiPath}/staff/create`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => <IUser>res.json())
            .catch((err: Error) => this.handleError(err))
    }


    getAll(filters?: any): Observable<IMember[]> {
        let url = `${GlobalService.apiPath}/staff/getAll`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, filters, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: Error) => this.handleError(err))
    }

    update(payload: any, id: string): Observable<IMember> {
        let url = `${GlobalService.apiPath}/staff/update/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.put(url, payload, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: Error) => this.handleError(err))
    }

    delete(id: string): Observable<IMember> {
        let url = `${GlobalService.apiPath}/staff/delete/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.delete(url, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: Error) => this.handleError(err))
    }

    addOrRemoveOfficeFromStaff(payload: any, method: string): Observable<any> {
        let url = '';
        switch (method) {
            case 'add':
                url = `${GlobalService.apiPath}/staff/add/office`;
                break;
            case 'remove':
                url = `${GlobalService.apiPath}/staff/delete/office`;
                break;
        }
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: Error) => this.handleError(err))
    }

    search(query: string): Observable<any> {
        let url = `${GlobalService.apiPath}/staff/search/${query}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IMember>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    searchByCompany(payload: any): Observable<IMember[]> {
        let url = `${GlobalService.apiPath}/staff/searchByCompany`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => <IMember[]>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    assignCompanyToMember(payload: any): Observable<any> {
        let url = `${GlobalService.apiPath}/staff/checkAssignToCompany`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => <IMember>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    handleError(error: any): any {
        return Observable.throw(error.json());
    }


}

@Injectable()
export class UserAuth implements CanActivate {
    isAuthentic: boolean;

    constructor(private _router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        localStorage.getItem('token') ? this.isAuthentic = true : (this.isAuthentic = false, this._router.navigate(['/']));
        return this.isAuthentic;
    }

}

@Injectable()
export class UserResolver implements Resolve<IMember> {

    constructor(private _http: Http) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IMember | Observable<IMember> | Promise<IMember> {
        let id = route.params['id'];
        let url = `${GlobalService.apiPath}/staff/get/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .toPromise()
            .then(res => res.json() as IMember)
            .catch(err => Promise.reject(err))
    }
}

@Injectable()
export class LoggedInUserResolver implements Resolve<IMember> {

    constructor(private _http: Http) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IMember | Observable<IMember> | Promise<IMember> {
        let url = `${GlobalService.apiPath}/staff/me`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .toPromise()
            .then(res => res.json() as IMember)
            .catch(err => Promise.reject(err))
    }
}
