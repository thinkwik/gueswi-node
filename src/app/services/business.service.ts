import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {GlobalService} from './global.service';
import {IBusiness} from '../models/business';
import {UserAuth} from './staff.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/throw';


@Injectable()
export class BusinessService {

    public currentBusiness: Subject<IBusiness> = new Subject();

    constructor(private _http: Http) {

    }

    create(payload: any): Observable<IBusiness> {
        let url = `${GlobalService.apiPath}/business/create`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => <IBusiness>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    update(payload: any, id: string): Observable<IBusiness> {
        let url = `${GlobalService.apiPath}/business/update/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.put(url, payload, {headers: headers})
            .map((res: Response) => <IBusiness>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    updateForAdmin(payload: any, id: string): Observable<IBusiness> {
        let url = `${GlobalService.apiPath}/business/updateForAdmin/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.put(url, payload, {headers: headers})
            .map((res: Response) => <IBusiness>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    get(id: string): Observable<IBusiness> {
        let url = `${GlobalService.apiPath}/business/get/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IBusiness>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    getAll(filters?: any): Observable<any> {
        let url = `${GlobalService.apiPath}/business/getAll`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, filters, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: Error) => this.handleError(err))
    }

    remove(id: string): Observable<IBusiness> {
        let url = `${GlobalService.apiPath}/business/delete/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.delete(url, {headers: headers})
            .map((res: Response) => <IBusiness>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    search(query: string): Observable<any> {
        let url = `${GlobalService.apiPath}/business/search/${query}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .map((res: Response) => <IBusiness>res.json())
            .catch((err: Error) => this.handleError(err))
    }

    handleError(error: any): any {
        return Observable.throw(error.json());
    }
}

@Injectable()
export class BusinessListResolver implements Resolve<IBusiness[]> {

    constructor(private _http: Http) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IBusiness[] | Observable<IBusiness[]> | Promise<IBusiness[]> {
        let url = `${GlobalService.apiPath}/business/getAll`;
        let headers = new Headers();
        let body = {limit: GlobalService.paginatonOptions.limit, offset: GlobalService.paginatonOptions.offset};
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, body, {headers: headers})
            .toPromise()
            .then(res => res.json() as IBusiness[])
            .catch(err => Promise.reject(err))
    }
}

@Injectable()
export class BusinessAuth implements CanActivate {
    isAuthentic: boolean;

    constructor(private _router: Router,
                private _auth: UserAuth) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!this._auth.canActivate(route, state)) {
            return false
        } else {
            localStorage.getItem('_bid') ? this.isAuthentic = true : (this.isAuthentic = false, this._router.navigate(['/']));
            return this.isAuthentic;
        }
    }

}

@Injectable()
export class BusinessResolver implements Resolve<IBusiness> {

    constructor(private _http: Http) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IBusiness | Observable<IBusiness> | Promise<IBusiness> {
        let id = localStorage.getItem('_bid') ? localStorage.getItem('_bid') : route.params.id;
        let url = `${GlobalService.apiPath}/business/get/${id}`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.get(url, {headers: headers})
            .toPromise()
            .then(res => res.json() as IBusiness)
            .catch(err => Promise.reject(err))
    }
}
