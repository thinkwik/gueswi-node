import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class FiltersService {

    public openSidebar: Subject<any> = new Subject();


    constructor() {

    }

    toggleSidebar(isOpen: boolean) {
        this.openSidebar.next(isOpen)
    }
}

