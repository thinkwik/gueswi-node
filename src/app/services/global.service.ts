import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GlobalService {

    // local
    public static apiPath = `http://localhost:5005/api`;

    // testing
    // public static apiPath = `http://162.243.255.48:3000/api`;

    // heroku
    // public static apiPath = `https://gueswi.herokuapp.com/api`;

    public static paginatonOptions = {
        limit: 9,
        offset: 0
    };

    constructor(private _http: Http) {

    }

    upload(payload: any): Observable<any> {
        let url = `${GlobalService.apiPath}/upload`;
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'));
        return this._http.post(url, payload, {headers: headers})
            .map((res: Response) => res.json())
            .catch((err: any) => Observable.throw(err.json()))
    }

    getBaseName(image: string): string {
        return image.replace(/\\/g, '/').replace(/.*\//, '');
    }

    getUniqueId(prefix: string): string {
        let uniqueId = prefix ? prefix + Math.floor(Date.now()) : Math.floor(Date.now()) + '';
        return uniqueId.toString().toUpperCase();
    }
}
