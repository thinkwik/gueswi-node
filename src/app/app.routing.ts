import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

/* COMPONENTS */
import {LoginComponent} from './components/login/login.component';
import {ForgotPasswordComponent} from './components/login/forgot-password.component';

const routes: Routes = [
    {
        path: '', redirectTo: 'login', pathMatch: 'full'
    },
    {
        path: 'login', component: LoginComponent
    },
    {
        path: 'forgot-password', component: ForgotPasswordComponent
    },
    {
        path: 'admin',
        loadChildren: './components/admin/admin.module#AdminModule',
    },
    {
        path: 'super-admin',
        loadChildren: './components/super-admin/super-admin.module#SuperAdminModule'
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule {
}