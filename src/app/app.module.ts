import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';
import {AppRoutingModule} from './app.routing';
import {SharedModule} from './components/shared/shared.module';
import {ToastrModule} from 'ngx-toastr';

/* TOASTER SETTINGS */
import {ToasterConfig} from './models/toaster';


/* COMPONENTS */
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {ForgotPasswordComponent} from './components/login/forgot-password.component';

/* SERVICES */
import {GlobalService} from './services/global.service';
import {StaffService} from './services/staff.service';

@NgModule({
    imports: [BrowserModule, BrowserAnimationsModule, NgbModule.forRoot(), HttpModule, AppRoutingModule, FormsModule, ReactiveFormsModule, SharedModule, ToastrModule.forRoot({
        timeOut: ToasterConfig.timeOut,
        positionClass: ToasterConfig.positionClass,
        preventDuplicates: ToasterConfig.preventDuplicates,
        tapToDismiss: ToasterConfig.tapToDismiss
    })],
    declarations: [AppComponent, LoginComponent, ForgotPasswordComponent],
    providers: [GlobalService, StaffService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
