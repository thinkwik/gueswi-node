import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

import {ProfileComponent} from './profile/profile.component';
import {ActivityComponent} from './activity/activity.component';
import {FiltersComponent} from './filters/filters.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [ProfileComponent, ActivityComponent, FiltersComponent],
    exports: [ProfileComponent, ActivityComponent, FiltersComponent]
})
export class SharedModule {

}
