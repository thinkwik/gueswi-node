import {Component, OnInit} from '@angular/core';
import {StaffService} from '../../../services/staff.service';
import {IUser} from '../../../models/user';
import {Router} from '@angular/router';

@Component({
    selector: 'profile',
    templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit {

    user: IUser;
    admin: string;

    constructor(private _staff: StaffService,
                private _router: Router) {
    }

    ngOnInit(): void {
        this._staff.user.subscribe(res => {
            this.user = res;
            if (this.user.role === 'super_user') {
                this.admin = 'super-admin';
            } else {
                this.admin = 'admin';
            }
        });
    }

    logout(): void {
        localStorage.removeItem('token');
        this._router.navigate(['/']);
    }
}
