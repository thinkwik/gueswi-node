import {Component} from '@angular/core';
import {StaffService} from '../../../services/staff.service';
import {IMember} from '../../../models/member';
import {Router} from '@angular/router';

@Component({
    templateUrl: 'super-admin-menu.component.html'
})
export class SuperAdminMenuComponent {

    user: IMember;

    constructor(private _staff: StaffService,
                private _router: Router) {
        this._staff.get().subscribe(res => {
            this.user = res;
            this._staff.user.next(res)
        }, err => console.log(err))
    }

    logout(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('_bid');
        this._router.navigate(['/']);
    }
}

