import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

/* COMPONENTS */

import {AdminHomeComponent} from './admin-home/admin-home.component';
import {SuperAdminMenuComponent} from './super-admin-menu/super-admin-menu.component';
import {CompanyListComponent} from './company/company-list.component';
import {CompanySearchComponent} from './company/company-search.component';
import {CompanyEditComponent} from './company/company-edit.component';
import {CompanyCreateComponent} from './company/company-create.component';
import {UserListComponent} from './user/users-list.component';
import {UsersSearchComponent} from './user/users-search.component';
import {UsersEditComponent} from './user/users-edit.component';
import {UserCreateComponent} from './user/users-create.component';
import {ExpiryListComponent} from './expirey/expiry-list.component';
import {AdminProFileEditComponent} from './admin-profile/admin-profile-edit.component';
// import {SuperAdminOfficeCreateComponent} from './office/super-admin.office.create.component';
// import {SuperAdminOfficeListComponent} from './office/super-admin.office.list.component';
// import {SuperAdminOfficeEditComponent} from './office/super-admin.office.edit.component';


import {BusinessResolver} from '../../services/business.service';
import {UserAuth, UserResolver} from '../../services/staff.service';

const routes: Routes = [
    {
        path: '',
        component: AdminHomeComponent,
        canActivate: [UserAuth],
        children: [
            {
                path: '',
                component: SuperAdminMenuComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'business',
                        pathMatch: 'full'
                    },
                    {
                        path: 'business',
                        children: [
                            {path: '', redirectTo: 'list', pathMatch: 'full'},
                            {path: 'list', component: CompanyListComponent},
                            {path: 'create', component: CompanyCreateComponent},
                            {path: 'search', component: CompanySearchComponent},
                            {path: 'edit/:id', component: CompanyEditComponent, resolve: {business: BusinessResolver}}
                        ]
                    },
                    {
                        path: 'users',
                        children: [
                            {path: '', redirectTo: 'list', pathMatch: 'full'},
                            {path: 'list', component: UserListComponent},
                            {path: 'create', component: UserCreateComponent},
                            {path: 'search', component: UsersSearchComponent},
                            {path: 'edit/:id', component: UsersEditComponent, resolve: {user: UserResolver}}
                        ]
                    },
                    {
                        path: 'expired',
                        children: [
                            {path: '', redirectTo: 'list', pathMatch: 'full'},
                            {path: 'list', component: ExpiryListComponent}
                        ]
                    }
                ]
            },
            {
                path: 'profile',
                component: AdminProFileEditComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {

}
