import {Component, OnInit} from '@angular/core';
import {IUser} from '../../../models/user';
import {FiltersService} from '../../../services/filters.service';

@Component({
    templateUrl: 'admin-home.component.html'
})
export class AdminHomeComponent implements OnInit {

    user: IUser;
    showFilters: boolean;

    constructor(private _filters: FiltersService) {
    }


    ngOnInit(): void {
      this._filters.openSidebar.subscribe(res => {
          this.showFilters = res;
      })
    }

    closeFilters(): void {
        this.showFilters = !this.showFilters;
        this._filters.openSidebar.next(this.showFilters);
    }

}
