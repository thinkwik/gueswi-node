import {Component, OnInit} from '@angular/core';
import {BusinessService} from '../../../services/business.service';
import {ActivatedRoute} from '@angular/router';
import {FiltersService} from '../../../services/filters.service';
import swal from 'sweetalert2';
import {GlobalService} from '../../../services/global.service';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: 'company-list.component.html'
})
export class CompanyListComponent implements OnInit {

    businesses: Observable<any>;
    filterObject: any = {
        limit: GlobalService.paginatonOptions.limit,
        offset: GlobalService.paginatonOptions.offset
    };
    total: number;
    page: any;
    showSideBar: boolean;

    constructor(private _business: BusinessService,
                private _filters: FiltersService,
                private _activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.showSideBar = false;
        this._filters.openSidebar.subscribe(res => {
            this.showSideBar = res;
        });
        this.getPage(1);
    };

    deleteBusiness(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this business?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let list = this._business.getAll(this.filterObject);
            let remove = this._business.remove(id);
            remove.switchMap(() => list).subscribe(res => {
                this.getPage(this.page);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    };

    toggleFilters(): void {
        this.showSideBar = !this.showSideBar;
        this._filters.toggleSidebar(this.showSideBar);
    }

    getPage(page: number) {
        this.filterObject.offset = this.filterObject.limit * (page - 1);
        this.businesses = this._business.getAll(this.filterObject)
            .do(res => {
                this.total = res.totalItems;
                this.page = page;
            }).map(res => res.data)
    }
}
