import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {IBusiness} from '../../../models/business';
import {BusinessService} from '../../../services/business.service';
import * as moment from 'moment';
import {GlobalService} from '../../../services/global.service';

@Component({
    templateUrl: 'company-edit.component.html'
})
export class CompanyEditComponent implements OnInit {

    business: IBusiness;
    businessFormGroup: FormGroup;
    office = 0;
    manager = 0;
    agent = 0;

    @ViewChild('image') image: ElementRef;
    selectedImage: any;

    constructor(private _business: BusinessService,
                private _global: GlobalService,
                private _formBuilder: FormBuilder,
                private _router: Router,
                private _activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { business: IBusiness }) => {
            this.business = res.business;
            this.office = this.business.office;
            this.manager = this.business.manager;
            this.agent = this.business.agent;
            this.buildBusinessData(this.business);
        });
    }

    updateBusiness(value: any): void {
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : '';
        value.office = this.office;
        value.manager = this.manager;
        value.agent = this.agent;
        value.contractStartDate = this.parseSimple(value.contractStartDate);
        value.contractEndDate = this.parseSimple(value.contractEndDate);
        let obs$ = this._business.update(value, this.business._id);
        obs$.subscribe(res => {
            this._router.navigate(['/', 'super-admin', 'business'])
        }, err => {
            console.log(err);
        })
    }


    buildBusinessData(business: IBusiness) {
        this.selectedImage = business ? business.image : '';
        this.businessFormGroup = this._formBuilder.group({
            name: [business ? business.name : '', Validators.required],
            tradeName: [business ? business.tradeName : '', Validators.required],
            location: [business ? business.location : '', Validators.required],
            phone: [business ? business.phone : '', Validators.required],
            nameOfRoad: [business ? business.nameOfRoad : '', Validators.required],
            CIF: [business ? business.CIF : '', Validators.required],
            numberOfWay: [business ? business.numberOfWay : '', Validators.required],
            contactPerson: [business ? business.contactPerson : '', Validators.required],
            contractStartDate: [business ? this.parseFormat(business.contractStartDate) : '', Validators.required],
            contractEndDate: [business ? this.parseFormat(business.contractEndDate) : '', Validators.required],
            salesPricePerOffice: [business ? business.salesPricePerOffice : '', Validators.required],
            password: [business ? business.password : '', Validators.required],
            email: [business ? business.email : '', Validators.required],
        });
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        if (file) {
            formData.append('image', file, file.name);
            let upload = this._global.upload(formData);
            upload.subscribe(res => {
                this.selectedImage = res.image;
            }, err => {
                console.log(err);
            })
        }

    }

    increase(model: string): void {
        switch (model) {
            case 'agent':
                this.agent += 1;
                break;
            case 'office':
                this.office += 1;
                break;
            case 'manager':
                this.manager += 1;
                break;
        }
    }

    decrease(model: string): void {
        switch (model) {
            case 'agent':
                this.agent > 0 ? this.agent -= 1 : false;
                break;
            case 'office':
                this.office > 0 ? this.office -= 1 : false;
                break;
            case 'manager':
                this.manager > 0 ? this.manager -= 1 : false;
                break;
        }
    }

    triggerUpload() {
        this.image.nativeElement.click();
    }

    parseFormat(obj: any): any {
        obj = moment(new Date(obj));
        let date = {year: 0, month: 0, day: 0};
        date.day = Number(obj.format('D'));
        date.month = Number(obj.format('M'));
        date.year = Number(obj.format('YYYY'));
        return date;
    }

    parseSimple(obj: any): any {
        return moment(new Date(`${obj.year}-${obj.month}-${obj.day}`));
    }
}
