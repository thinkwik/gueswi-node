import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from '../../../services/business.service';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {GlobalService} from '../../../services/global.service';
import {StaffService} from '../../../services/staff.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    templateUrl: 'company-create.component.html'
})
export class CompanyCreateComponent implements OnInit {


    businessFormGroup: FormGroup;
    userFormGroup: FormGroup;
    selectedImage: any;
    selectedUserImage: string;
    showUserForm: boolean;
    businessId: string;
    office: any = 0;
    manager: any = 0;
    agent: any = 0;


    @ViewChild('image') image: ElementRef;
    @ViewChild('userImage') userImage: ElementRef;
    @ViewChild('logo') logo: ElementRef;

    constructor(private _formBuilder: FormBuilder,
                private _router: Router,
                private _toast: ToastrService,
                private _staff: StaffService,
                private _global: GlobalService,
                private _business: BusinessService) {

    }

    ngOnInit(): void {
        this.showUserForm = false;
        this.buildBusinessForm();
        this.buildUserFormGroup();
    }

    buildBusinessForm(): void {
        this.businessFormGroup = this._formBuilder.group({
            name: ['', Validators.required],
            tradeName: ['', Validators.required],
            location: ['', Validators.required],
            phone: ['', Validators.required],
            nameOfRoad: ['', Validators.required],
            CIF: ['', Validators.required],
            numberOfWay: ['', Validators.required],
            contactPerson: ['', Validators.required],
            contractStartDate: ['', Validators.required],
            contractEndDate: ['', Validators.required],
            salesPricePerOffice: ['', Validators.required],
            password: ['', Validators.required],
            email: ['', Validators.required]
        })
    }

    buildUserFormGroup(): void {
        this.userFormGroup = this._formBuilder.group({
            telephone: ['', Validators.required],
            position: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            password: ['', Validators.required],
            email: ['', Validators.required],
            tags: [],
            address: ['']
        })
    }

    triggerUpload(place: string) {
        switch (place) {
            case 'company':
                this.image.nativeElement.click();
                break;
            case 'user':
                this.userImage.nativeElement.click();
                break;
        }
    }

    changeFile(event: any, section?: string): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            if (section === 'profile') {
                this.selectedUserImage = res.image;
            } else {
                this.selectedImage = res.image;
            }
        }, err => {
            console.log(err);
        })
    }

    addBusiness(value: any): void {
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : '';
        value.office = this.office;
        value.manager = this.manager;
        value.agent = this.agent;
        value.contractStartDate = this.parseFormat(value.contractStartDate);
        value.contractEndDate = this.parseFormat(value.contractEndDate);
        let obs$ = this._business.create(value);
        obs$.subscribe(res => {
            this.businessId = res._id;
            this.showUserForm = true;
        }, err => {
            console.log(err);
        })
    }

    createStaffMember(value: any): void {
        value.image = this.selectedUserImage ? this._global.getBaseName(this.selectedUserImage) : '';
        value.tags = value.tags ? value.tags = value.tags.map((item: any) => item.display) : [];
        value.company = this.businessId ? this.businessId : '';
        value.role = 'admin';
        let staff = this._staff.create(value);
        staff.subscribe(res => {
            this._router.navigate(['/', 'super-admin', 'business']);
            this._toast.success('Business Created Successfully', 'Success');
        }, err => {
            this._toast.success(err.message.toString(), 'Error');
        })
    }

    increase(model: string): void {
        switch (model) {
            case 'agent':
                this.agent += 1;
                break;
            case 'office':
                this.office += 1;
                break;
            case 'manager':
                this.manager += 1;
                break;
        }
    }

    decrease(model: string): void {
        switch (model) {
            case 'agent':
                this.agent > 0 ? this.agent -= 1 : false;
                break;
            case 'office':
                this.office > 0 ? this.office -= 1 : false;
                break;
            case 'manager':
                this.manager > 0 ? this.manager -= 1 : false;
                break;
        }
    }

    parseFormat(obj: any): any {
        return moment(`${obj.year}-${obj.month}-${obj.day}`).format('YYYY-MM-DD');
    }
}

