import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {BusinessService} from '../../../services/business.service';
import swal from 'sweetalert2';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';

@Component({
    templateUrl: 'company-search.component.html'
})
export class CompanySearchComponent implements OnInit {

    businesses: Observable<any>;
    searchFormGroup: FormGroup;
    query: string;

    constructor(private _formBuilder: FormBuilder,
                private _business: BusinessService) {

    }

    ngOnInit(): void {
        this.buildSearchForm();
        this.getAllBusinesses();
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.query === '' || res.query === undefined)) {
                this.doSearch(res.query);
            } else {
                this.getAllBusinesses();
            }
        })
    }

    getAllBusinesses() {
        this.businesses = this._business.getAll().map(res => res.data);
    }

    buildSearchForm() {
        this.searchFormGroup = this._formBuilder.group({
            query: ['']
        })
    }

    doSearch(query: string): void {
        this.query = query;
        this.businesses = this._business.search(query)
            .debounceTime(300)
            .distinctUntilChanged()
    }

    deleteBusiness(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this business?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let remove = this._business.remove(id);
            remove.subscribe(res => {
                this.doSearch(this.query);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    };
}
