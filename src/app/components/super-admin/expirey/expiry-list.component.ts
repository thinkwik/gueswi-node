import {Component, OnInit} from '@angular/core';
import {FiltersService} from '../../../services/filters.service';
import {ActivatedRoute} from '@angular/router';
import {IBusiness} from '../../../models/business';
import * as moment from 'moment';
import swal from 'sweetalert2';
import {BusinessService} from '../../../services/business.service';
import {GlobalService} from '../../../services/global.service';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: 'expiry-list.component.html'
})
export class ExpiryListComponent implements OnInit {

    showSideBar: boolean;
    businesses: Observable<any>;
    page: number;
    total: any;
    filterObject: any = {
        limit: GlobalService.paginatonOptions.limit,
        offset: GlobalService.paginatonOptions.offset
    };

    constructor(private _filters: FiltersService,
                private _business: BusinessService) {

    }

    ngOnInit(): void {
        this.showSideBar = false;
        this._filters.openSidebar.subscribe(res => {
            this.showSideBar = res;
        });
        this.getPage(1);
    }

    toggleFilters(): void {
        this.showSideBar = !this.showSideBar;
        this._filters.toggleSidebar(this.showSideBar);
    }

    deleteBusiness(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this business?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let list = this._business.getAll(this.filterObject);
            let remove = this._business.remove(id);
            remove.switchMap(() => list).subscribe(res => {
                this.getPage(this.page);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    };

    getPage(page: number) {
        this.filterObject.offset = this.filterObject.limit * (page - 1);
        this.getAllBusinesses(page);
    }

    getDiffDays(endDate: any): any {
        let startDate = moment(new Date());
        endDate = moment(new Date(endDate));
        return endDate.diff(startDate, 'days')
    }

    getAllBusinesses(page?: any): void {
        this.businesses = this._business.getAll(this.filterObject)
            .do(res => {
                this.total = res.totalItems;
                this.page = page;
            }).map(res => res.data)
    }
}
