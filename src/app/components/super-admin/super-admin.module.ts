import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminRoutingModule} from './super-admin.routing.module';
import {SharedModule} from '../shared/shared.module';
import {NgbDatepickerModule, NgbModalModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {TagInputModule} from 'ngx-chips';
import {NgxPaginationModule} from 'ngx-pagination';
/* COMPONENTS  */
import {SuperAdminMenuComponent} from './super-admin-menu/super-admin-menu.component';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {CompanyListComponent} from './company/company-list.component';
import {CompanySearchComponent} from './company/company-search.component';
import {CompanyCreateComponent} from './company/company-create.component';
import {CompanyEditComponent} from './company/company-edit.component';
import {UserListComponent} from './user/users-list.component';
import {UsersSearchComponent} from './user/users-search.component';
import {UserCreateComponent} from './user/users-create.component';
import {UsersEditComponent} from './user/users-edit.component';
import {ExpiryListComponent} from './expirey/expiry-list.component';
import {AdminProFileEditComponent} from './admin-profile/admin-profile-edit.component';
/* SERVICES */
import {BusinessListResolver, BusinessResolver, BusinessService} from '../../services/business.service';
import {OfficeService} from '../../services/office.service';
import {ManagerListResolver, ManagerService} from '../../services/manager.service';
import {FiltersService} from '../../services/filters.service';
import {UserAuth, UserResolver} from '../../services/staff.service';

@NgModule({
    imports: [AdminRoutingModule, CommonModule, FormsModule, ReactiveFormsModule, SharedModule, TagInputModule, NgxPaginationModule, NgbModalModule, NgbDropdownModule, NgbDatepickerModule],
    declarations: [SuperAdminMenuComponent, AdminHomeComponent, CompanyListComponent, CompanySearchComponent, CompanyCreateComponent, CompanyEditComponent, UserListComponent, UsersEditComponent, UserCreateComponent, UsersSearchComponent, ExpiryListComponent, AdminProFileEditComponent],
    providers: [BusinessService, BusinessListResolver, BusinessResolver, OfficeService, ManagerService, ManagerListResolver, FiltersService, UserResolver, UserAuth]
})

export class SuperAdminModule {

}
