import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup} from '@angular/forms';
import {StaffService} from '../../../services/staff.service';
import swal from 'sweetalert2';

@Component({
    templateUrl: 'users-search.component.html'
})
export class UsersSearchComponent implements OnInit {

    users: Observable<any>;
    searchFormGroup: FormGroup;
    query: string;

    constructor(private _formBuilder: FormBuilder,
                private _staff: StaffService) {

    }

    ngOnInit(): void {
        this.buildSearchForm();
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.query === '' || res.query === undefined)) {
                this.doSearch(res.query);
            }

        })
    }

    buildSearchForm() {
        this.searchFormGroup = this._formBuilder.group({
            query: ['']
        })
    }

    doSearch(query: string): void {
        this.query = query;
        this.users = this._staff.search(query)
            .debounceTime(300)
            .distinctUntilChanged()
    }


    deleteUser(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this User?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let remove = this._staff.delete(id);
            remove.subscribe(res => {
                this.doSearch(this.query);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    }
}
