import {Component, OnInit} from '@angular/core';
import {IMember} from '../../../models/member';
import {FiltersService} from '../../../services/filters.service';
import {GlobalService} from '../../../services/global.service';
import {StaffService} from '../../../services/staff.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
    templateUrl: 'users-list.component.html'
})
export class UserListComponent implements OnInit{

    users: Observable<IMember[]>;
    showSideBar: boolean;
    filterObject: any = {
        limit: GlobalService.paginatonOptions.limit,
        offset: GlobalService.paginatonOptions.offset
    };
    total: number;
    page: any;

    constructor(private _filters: FiltersService,
                private _staff: StaffService) {
    }

    ngOnInit(): void {
        this.showSideBar = false;
        this._filters.openSidebar.subscribe(res => {
            this.showSideBar = res;
        });
        this.getPage(1);
    }

    deleteUser(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this User?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let list = this._staff.getAll(this.filterObject);
            let remove = this._staff.delete(id);
            remove.switchMap(() => list).subscribe(res => {
                this.getPage(this.page);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    }

    toggleFilters(): void {
        this.showSideBar = !this.showSideBar;
        this._filters.toggleSidebar(this.showSideBar);
    }

    getPage(page: number) {
        this.filterObject.offset = this.filterObject.limit * (page - 1);
        this.users = this._staff.getAll(this.filterObject)
            .do(res => {
                this.total = res['totalItems'];
                this.page = page;
            }).map(res => res['data'])
    }


}
