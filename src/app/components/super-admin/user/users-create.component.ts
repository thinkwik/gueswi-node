import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../services/global.service';
import {StaffService} from '../../../services/staff.service';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BusinessService} from '../../../services/business.service';
import {Observable} from 'rxjs/Observable';
import {IBusiness} from '../../../models/business';

@Component({
    templateUrl: 'users-create.component.html'
})
export class UserCreateComponent implements OnInit {

    businesses: Observable<IBusiness[]>;
    userFormGroup: FormGroup;
    position: string;
    company: any = {name: '', id: ''};
    selectedUserImage: string;
    errorMessage: string;
    searchFormGroup: FormGroup;
    @ViewChild('userImage') userImage: ElementRef;

    constructor(private _formBuilder: FormBuilder,
                private _staff: StaffService,
                private _business: BusinessService,
                private _model: NgbModal,
                private _router: Router,
                private _global: GlobalService) {

    }

    ngOnInit(): void {
        this.position = 'admin';
        this.getAllBusinesses();
        this.buildUserFormGroup();
        this.searchFormGroup = this._formBuilder.group({searchBusinessName: ['']});
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.searchBusinessName === undefined || res.searchBusinessName === '')) {
                this.doSearch(res.searchBusinessName)
            } else {
                this.getAllBusinesses();
            }
        })
    }

    triggerUpload() {
        this.userImage.nativeElement.click();
    }

    openModel(content: any): void {
        this._model.open(content, {size: 'lg'});
    }

    buildUserFormGroup(): void {
        this.userFormGroup = this._formBuilder.group({
            telephone: ['', Validators.required],
            position: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            password: ['', Validators.required],
            email: ['', Validators.required],
            tags: [],
            address: [''],
            company: ['', Validators.required]
        })
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedUserImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    createStaffMember(value: any): void {
        value.image = this.selectedUserImage ? this._global.getBaseName(this.selectedUserImage) : '';
        value.tags = value.tags ? value.tags = value.tags.map((item: any) => item.display) : [];
        value.role = this.position;
        value.company = this.company.id;
        let staff = this._staff.create(value);
        staff.subscribe(res => {
            this.userFormGroup.reset();
            this._router.navigate(['/', 'super-admin', 'users']);
        }, err => {
            console.log(err);
        })
    }

    getAllBusinesses() {
        this.businesses = this._business.getAll().map(res => res.data);
    }

    doSearch(query: any): void {
        this.businesses = this._business.search(query)
            .debounceTime(300)
            .distinctUntilChanged()
    }

    checkForAvailability(companyId: string, name: string): void {
        let body = {
            memberRole: this.position,
            companyId
        };
        this._staff.assignCompanyToMember(body).subscribe(res => {
            this.company.id = companyId;
            this.company.name = name;
            this.userFormGroup.get('company').setValue(this.company.name);
       }, err => {
            this.errorMessage = err.message;
            setTimeout(() => {
                this.errorMessage = ''
            }, 4000)
        })

    }
}