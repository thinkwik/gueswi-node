import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {GlobalService} from '../../../services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StaffService} from '../../../services/staff.service';
import {IMember} from '../../../models/member';
import * as _ from 'lodash';

@Component({
    templateUrl: 'users-edit.component.html'
})
export class UsersEditComponent implements OnInit {


    @ViewChild('userImage') userImage: ElementRef;
    userFormGroup: FormGroup;
    member: IMember;
    role: string;
    selectedUserImage: string;

    constructor(private _formBuilder: FormBuilder,
                private _staff: StaffService,
                private _router: Router,
                private _activatedRoute: ActivatedRoute,
                private _global: GlobalService) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { user: IMember }) => {
            this.member = res.user;
            console.log(this.member);
            this.role = this.member.role;
            this.selectedUserImage = this.member.image;
            this.buildUserForm(this.member);
        })
    }

    triggerUpload() {
        this.userImage.nativeElement.click();
    }


    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedUserImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    buildUserForm(user: IMember): void {
        this.userFormGroup = this._formBuilder.group({
            telephone: [user.telephone ? user.telephone : ''],
            position: [user.position ? user.position : ''],
            firstName: [user.firstName ? user.firstName : ''],
            lastName: [user.lastName ? user.lastName : ''],
            password: [user.password ? user.password : ''],
            email: [user.email ? user.email : ''],
            tags: [user.tags ? user.tags : ''],
            address: [user.address ? user.address : '']
        })
    }

    updateUser(value: any): void {
        value.image = this.selectedUserImage ? this._global.getBaseName(this.selectedUserImage) : '';
        value.role = this.role;
        let modifiedTags: any = [];
        if (value.tags) { _.each(value.tags, tag => typeof tag === 'string' ? modifiedTags.push(tag) : modifiedTags.push(tag['display'])) }
        value.tags = modifiedTags;
        let staff = this._staff.update(value, this.member._id);
        staff.subscribe(res => {
            this.userFormGroup.reset();
            this._router.navigate(['/', 'super-admin', 'users']);
        }, err => {
            console.log(err);
        })
    }
}
