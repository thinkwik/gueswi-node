import {Component, OnInit} from '@angular/core';
import {StaffService} from '../../services/staff.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

    loginFormGroup: FormGroup;
    errorMessage: string;

    constructor(private _staff: StaffService,
                private _router: Router,
                private _formBuilder: FormBuilder) {

    }

    ngOnInit(): void {
        this.buildLoginForm();
    }

    buildLoginForm(): void {
        this.loginFormGroup = this._formBuilder.group({
            email: ['', Validators.compose([Validators.required, Validators.email])],
            password: ['', Validators.required]
        })
    }

    loginUser(value: any): void {
        let login$ = this._staff.login(value);
        login$.subscribe(res => {
            this._staff.user.next(res['data']);
            if (res['data'].role === 'super_user') {
                this._router.navigate(['/', 'super-admin']);
            } else {
                this._router.navigate(['/', 'admin']);
            }
        }, err => {
            this.errorMessage = err.message;
            setTimeout(() => {
                this.errorMessage = '';
            }, 4000)
        })
    }
}
