import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AdminHomeComponent} from './dashboard/admin-home.component';
import {OverviewComponent} from './overview/overview.component';
// import {UserAuth} from '../../services/staff.service';
import {ProfileComponent} from './profile/profile.component';
import {BusinessAuth} from '../../services/business.service';

const routes: Routes = [
    {
        path: '',
        component: AdminHomeComponent,
        canActivate: [BusinessAuth],
        children: [
            {
              path: '',
              redirectTo: 'overview',
              pathMatch: 'full'
            },
            {
                path: 'overview',
                component: OverviewComponent,
            },
            {
                path: 'business',
                loadChildren: './business/business.module#BusinessModule'
            },
            {
                path: 'profile',
                component: ProfileComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {

}
