import {Component} from '@angular/core';
import {StaffService} from '../../../services/staff.service';
import {IMember} from '../../../models/member';


@Component({
    templateUrl: `admin-home.component.html`
})
export class AdminHomeComponent {
    user: IMember;

    constructor(private _staff: StaffService) {
        this._staff.get().subscribe(res => {
            this.user = res;
            this._staff.user.next(res)
        }, err => console.log(err))
    }
}
