import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {GlobalService} from '../../../../services/global.service';
import {OfficeService} from '../../../../services/office.service';
import {StaffService} from '../../../../services/staff.service';
import {IMember} from '../../../../models/member';

@Component({
    templateUrl: 'manager-edit.component.html'
})
export class ManagerEditComponent {


    @ViewChild('userImage') userImage: ElementRef;
    userFormGroup: FormGroup;
    searchFormGroup: FormGroup;
    selectedImage: string;
    offices: any;
    member: IMember;
    assignedOffices: any[] = [];

    constructor(private _global: GlobalService,
                private _modal: NgbModal,
                private _router: Router,
                private _office: OfficeService,
                private _staff: StaffService,
                private _toast: ToastrService,
                private _activatedRoute: ActivatedRoute,
                private _formBuilder: FormBuilder) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { user: IMember }) => {
            this.member = res.user;
            this.assignedOffices = this.member.offices;
            this.selectedImage = this.member.image;
            this.buildUserFormGroup(this.member);
            this.buildSearchForm();
            this.getAllOffices();
        });
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.searchOfficeName === undefined || res.searchOfficeName === '')) {
                this.doSearchOffice(res.searchOfficeName)
            } else {
                this.getAllOffices();
            }
        });
    }

    buildUserFormGroup(member: IMember) {
        this.userFormGroup = this._formBuilder.group({
            telephone: [member.telephone ? member.telephone : '', Validators.required],
            position: [member.position ? member.position : '', Validators.required],
            firstName: [member.firstName ? member.firstName : '', Validators.required],
            lastName: [member.lastName ? member.lastName : '', Validators.required],
            password: [member.password ? member.password : '', Validators.required],
            email: [member.email ? member.email : '', Validators.required],
            tags: [member.tags ? member.tags : []],
            address: [member.address ? member.address : '']
        })
    }

    buildSearchForm() {
        this.searchFormGroup = this._formBuilder.group({
            searchOfficeName: ['']
        })
    }

    triggerUpload() {
        this.userImage.nativeElement.click();
    }

    openModel(content: any): void {
        this._modal.open(content, {size: 'lg'}).result.then(res => console.log(res), err => console.log(err));
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    createManager(value: any): void {
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : '';
        let modifiedTags: any = [];
        if (value.tags) {
            for (let tag of value.tags) {
                typeof tag === 'string' ? modifiedTags.push(tag) : modifiedTags.push(tag['display'])
            }
        }
        value.tags = modifiedTags;
        value.role = 'manager';
        value.company = this.member.company._id;
        value.offices = this.assignedOffices ? this.assignedOffices = this.assignedOffices.map((item: any) => item._id) : [];
        let create = this._staff.update(value, this.member._id);
        create.subscribe(res => {
            this._router.navigate(['/', 'admin', 'business', 'managers', 'list']);
            this._toast.success('Manager Updated', 'Success!');
        }, err => {
            this._toast.error(err.message.toString(), 'Updation Failed!', {timeOut: 5000});
        });
    }

    getAllOffices() {
        this._office.getOfficesByCompany('id', this.member.company._id).subscribe(res => this.offices = res)
    }

    addOrRemoveOffice(office: any, index: number): void {
        let i = this.assignedOffices.findIndex(item => item.id === office._id);
        if (i === -1) {
            this.assignedOffices.push({_id: office._id, officeName: office.officeName});
            office['selected'] = true;
            this.offices.splice(index, 1, office);
            this.addOrRemoveOfficeFromManager(this.member._id, office._id, 'add');
        } else {
            this.assignedOffices.splice(i, 1);
            if (office.selected) delete office.selected;
            this.offices.splice(index, 1, office);
            this.addOrRemoveOfficeFromManager(this.member._id, office._id, 'remove');
        }
    }

    removeOffice(office: any, i: number) {
        this.assignedOffices.splice(i, 1);
        let sOffice = this.offices.find((item: any) => item._id === office._id);
        delete sOffice.selected;
        let index = this.offices.findIndex((item: any) => item._id === sOffice._id);
        this.offices.splice(index, 1, sOffice);
        this.addOrRemoveOfficeFromManager(this.member._id, office._id, 'remove');
    }


    doSearchOffice(name: any): void {
        let query = {
            name,
            company: this.member.company._id
        };
        this._office.getOfficesByCompany('name', query).subscribe(res => this.offices = res);
    }

    addOrRemoveOfficeFromManager(personId, officeId, method, role = this.member.role): void {
        let body = {
            personId,
            officeId,
            role
        };
        this._staff.addOrRemoveOfficeFromStaff(body, method).subscribe(() => {
            if (method === 'add') this._toast.success(`Office Added! Manager Updated.`, 'Success');
            if (method === 'remove') this._toast.success(`Office Removed! Manager Updated.`, 'Success');
        }, err => {
            this._toast.success(err.message, 'Error');
        })
    }

    trackByFn(index: number, item: any) {
        return index;
    }
}
