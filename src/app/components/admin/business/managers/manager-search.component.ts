import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {FormBuilder, FormGroup} from '@angular/forms';
import {StaffService} from '../../../../services/staff.service';
import swal from 'sweetalert2';

@Component({
    templateUrl: 'manager-search.component.html'
})
export class ManagerSearchComponent implements OnInit {

    users: Observable<any>;
    searchFormGroup: FormGroup;
    filters: any;

    constructor(private _formBuilder: FormBuilder,
                private _staff: StaffService) {

    }

    ngOnInit(): void {
        this.buildSearchForm();
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.query === '' || res.query === undefined)) {
                this.doSearch(res.query);
            } else {
                let filters = {
                    role: 'manager',
                    company: localStorage.getItem('_bid')
                };
                this.users = this._staff.getAll(filters);
            }
        })
    }

    buildSearchForm() {
        this.searchFormGroup = this._formBuilder.group({
            query: ['']
        })
    }

    doSearch(query: string): void {
        this.filters = {
            company: localStorage.getItem('_bid'),
            role: 'manager',
            name: query
        };
        this.users = this._staff.searchByCompany(this.filters)
            .debounceTime(300)
            .distinctUntilChanged()
    }


    deleteUser(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this User?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let remove = this._staff.delete(id);
            remove.subscribe(res => {
                this.doSearch(this.filters);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    }
}
