import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';
import {GlobalService} from '../../../../services/global.service';
import {StaffService} from '../../../../services/staff.service';
import {IBusiness} from '../../../../models/business';
import {IMember} from '../../../../models/member';

@Component({
    templateUrl: 'manager-list.component.html'
})
export class ManagerListComponent implements OnInit {

    activeBusiness: IBusiness;
    users: Observable<IMember[]>;
    filterObject: any = {
        limit: GlobalService.paginatonOptions.limit,
        offset: GlobalService.paginatonOptions.offset
    };
    total: number;
    page: any;

    constructor(private _activatedRoute: ActivatedRoute,
                private _staff: StaffService) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { business: IBusiness }) => {
            this.activeBusiness = res.business;
            this.activeBusiness.staff = this.activeBusiness.staff.filter(staff => staff.role === 'manager');
        });
        this.getPage(1);
    }

    deleteUser(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this User?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let list = this._staff.getAll(this.filterObject);
            let remove = this._staff.delete(id);
            remove.switchMap(() => list).subscribe(res => {
                this.getPage(this.page);
            }, err => {
                console.log(err.message);
            });
        }).catch(err => {
            console.log(err.message)
        })
    }

    getPage(page: number) {
        this.filterObject.offset = this.filterObject.limit * (page - 1);
        this.filterObject.role = 'manager';
        this.filterObject.company = this.activeBusiness._id;
        this.users = this._staff.getAll(this.filterObject)
            .do(res => {
                this.total = res['totalItems'];
                this.page = page;
            }).map(res => res['data'])
    }
}
