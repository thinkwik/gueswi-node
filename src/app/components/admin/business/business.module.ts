import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BusinessRoutingModule} from './business.routing.module';
import {SharedModule} from '../../shared/shared.module';
import {NgbDropdownModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {TagInputModule} from 'ngx-chips';
import {NgxPaginationModule} from 'ngx-pagination';

/* COMPONENTS */
import {BusinessHomeComponent} from './business-home/business-home.component';
import {CompanyComponent} from './company/company.component';
import {AgentListComponent} from './agents/agent-list.component';
import {AgentSearchComponent} from './agents/agent-search.component';
import {AgentCreateComponent} from './agents/agent-create.component';
import {AgentEditComponent} from './agents/agent-edit.component';
import {ManagerListComponent} from './managers/manager-list.component';
import {ManagerSearchComponent} from './managers/manager-search.component';
import {ManagerCreateComponent} from './managers/manager-create.component';
import {ManagerEditComponent} from './managers/manager-edit.component';
import {OfficeCreateComponent} from './offices/office-create.component';
import {OfficeEditComponent} from './offices/office-edit.component';
import {OfficeListComponent} from './offices/office-list.component';
import {OfficeSearchComponent} from './offices/office-search.component';
import {RatingListComponent} from './ratings/rating-list.component';
import {RatingCreateComponent} from './ratings/rating-create.component';
import {RatingEditComponent} from './ratings/rating-edit.component';
import {RecruitmentIndexComponent} from './recruitment/recruitment.index.component';
import {WhatsappIndexComponent} from './whatsapp/whatsapp.index.component';


/* SERVICES */
import {BusinessResolver, BusinessAuth} from '../../../services/business.service';
import {LoggedInUserResolver, UserAuth, UserResolver} from '../../../services/staff.service';
import {OfficeService, OfficeResolver} from '../../../services/office.service';


@NgModule({
    declarations: [BusinessHomeComponent, CompanyComponent, AgentListComponent, AgentSearchComponent, AgentCreateComponent, AgentEditComponent, ManagerListComponent, ManagerSearchComponent, ManagerCreateComponent, ManagerEditComponent, OfficeCreateComponent, OfficeEditComponent, OfficeListComponent, OfficeSearchComponent, RatingCreateComponent, RatingListComponent, RatingEditComponent,
        RecruitmentIndexComponent, WhatsappIndexComponent],
    imports: [CommonModule, TagInputModule, FormsModule, ReactiveFormsModule, NgbModalModule, NgbDropdownModule, NgxPaginationModule, BusinessRoutingModule, SharedModule],
    providers: [OfficeService, LoggedInUserResolver, BusinessResolver, BusinessAuth, UserAuth, UserResolver, OfficeResolver]
})
export class BusinessModule {

}
