import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BusinessService} from '../../../../services/business.service';
import {IBusiness} from '../../../../models/business';
import {ActivatedRoute, Router} from '@angular/router';
import {IMember} from '../../../../models/member';
import {GlobalService} from '../../../../services/global.service';
import {ToastrService} from 'ngx-toastr';
import {OfficeService} from '../../../../services/office.service';
import {StaffService} from '../../../../services/staff.service';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';

@Component({
    templateUrl: 'office-create.component.html'
})
export class OfficeCreateComponent implements OnInit {

    officeFormGroup: FormGroup;
    activeBusiness: IBusiness;
    uniqueId: string;
    selectedImage: string;

    // for db
    agents: any[] = [];
    managers: any[] = [];

    // for model
    agentList: IMember[];
    managersList: IMember[];

    @ViewChild('officeImage') officeImage: ElementRef;

    managerSearchFormGroup: FormGroup;
    agentSearchFormGroup: FormGroup;

    constructor(private _formBuilder: FormBuilder,
                private _router: Router,
                private _activatedRoute: ActivatedRoute,
                private _modal: NgbModal,
                private _business: BusinessService,
                private _staff: StaffService,
                private _toast: ToastrService,
                private _global: GlobalService,
                private _office: OfficeService) {
    }

    ngOnInit(): void {
        this.uniqueId = this._global.getUniqueId('OF');
        this.buildOfficeForm();
        this.buildManagerSearchForm();
        this.buildAgentSearchForm();
        this._activatedRoute.data.subscribe((res: { business: IBusiness }) => {
            this.activeBusiness = res.business;
            this.getAllManagers();
            this.getAllAgents();
            this._business.currentBusiness.next(res.business);
        });

        this._business.currentBusiness.subscribe(res => {
            this.activeBusiness = res;
        });

        this.managerSearchFormGroup.valueChanges.subscribe(res => {
            if (!(res.managerQuery === '' || res.managerQuery === undefined)) {
                this.doSearchManagers(res.managerQuery);
            } else {
                this.getAllManagers();
            }
        });

        this.agentSearchFormGroup.valueChanges.subscribe(res => {
            if (!(res.agentQuery === '' || res.agentQuery === undefined)) {
                this.doSearchAgents(res.agentQuery);
            } else {
                this.getAllAgents();
            }
        });

    }

    openManagersModel(content: any): void {
        this._modal.open(content, {size: 'lg'}).result.then(res => console.log(res), err => console.log(err))
    }

    triggerUpload(): void {
        this.officeImage.nativeElement.click();
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    buildOfficeForm(): void {
        this.officeFormGroup = this._formBuilder.group({
            officeName: ['', Validators.required],
            location: ['', Validators.required],
            tags: [''],
            nameOfRoad: ['', Validators.required],
            numberOfWay: ['', Validators.required]
        });
    }

    getAllManagers(): void {
        let filters = {
            role: 'manager',
            company: this.activeBusiness._id
        };
        this._staff.getAll(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .map(res => res['data'])
            .subscribe(res => this.managersList = res, err => console.log(err));
    }

    buildManagerSearchForm(): void {
        this.managerSearchFormGroup = this._formBuilder.group({
            managerQuery: ['']
        })
    }

    addOrRemoveManager(member: any, index: number): void {
        let i = this.managers.findIndex(item => item._id === member._id);
        if (i === -1) {
            this.managers.push({_id: member._id, firstName: member.firstName, lastName: member.lastName});
            member['selected'] = true;
            this.managersList.splice(index, 1, member);
        } else {
            this.managers.splice(i, 1);
            if (member.selected) delete member.selected;
            this.managersList.splice(index, 1, member);
        }
    }

    removeManager(member: any, i: number): void {
        this.managers.splice(i, 1);
        let sManager = this.managersList.find((item: any) => item._id === member._id);
        delete sManager['selected'];
        let index = this.managersList.findIndex((item: any) => item._id === sManager._id);
        this.managersList.splice(index, 1, sManager);
    }

    getAllAgents(): void {
        let filters = {
            role: 'agent',
            company: this.activeBusiness._id
        };
        this._staff.getAll(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .map(res => res['data'])
            .subscribe(res => this.agentList = res, err => console.log(err));
    }

    buildAgentSearchForm(): void {
        this.agentSearchFormGroup = this._formBuilder.group({
            agentQuery: ['']
        })
    }

    addOrRemoveAgent(member: any, index: number): void {
        let i = this.agents.findIndex(item => item._id === member._id);
        if (i === -1) {
            this.agents.push({_id: member._id, firstName: member.firstName, lastName: member.lastName});
            member['selected'] = true;
            this.agentList.splice(index, 1, member);
        } else {
            this.agents.splice(i, 1);
            if (member.selected) delete member.selected;
            this.agentList.splice(index, 1, member);
        }
    }

    removeAgent(member: any, i: number): void {
        this.agents.splice(i, 1);
        let sAgent = this.agentList.find((item: any) => item._id === member._id);
        delete sAgent['selected'];
        let index = this.agentList.findIndex((item: any) => item._id === sAgent._id);
        this.agentList.splice(index, 1, sAgent);
    }

    createOffice(value: any): void {
        value.uniqueId = this.uniqueId;
        value.agents = this.agents ? this.agents = this.agents.map((item: any) => item._id) : [];
        value.managers = this.managers ? this.managers = this.managers.map((item: any) => item._id) : [];
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : '';
        value.tags = value.tags ? value.tags = value.tags.map((item: any) => item.display) : [];
        value.company = this.activeBusiness._id;
        let office = this._office.create(value);
        office.subscribe(res => {
            this._toast.success('Office Added Successfully', 'Success');
            this._router.navigate(['/', 'admin', 'business', 'offices'])
        }, err => {
            this._toast.error(err.message.toString(), 'Something went wrong!', {timeOut: 5000});
        });
    }

    doSearchManagers(query: string): void {
        let filters = {
            company: this.activeBusiness._id,
            role: 'manager',
            name: query
        };
        this._staff.searchByCompany(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe(res => this.managersList = res, err => console.log(err));
    }

    doSearchAgents(query: string): void {
        let filters = {
            company: this.activeBusiness._id,
            role: 'agent',
            name: query
        };
        this._staff.searchByCompany(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe(res => this.agentList = res, err => console.log(err));
    }

    trackByFn(index: number, item: any) {
        return index;
    }
}
