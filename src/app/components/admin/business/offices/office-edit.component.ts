import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BusinessService} from '../../../../services/business.service';
import {IBusiness} from '../../../../models/business';
import {ActivatedRoute, Router} from '@angular/router';
import {IMember} from '../../../../models/member';
import {GlobalService} from '../../../../services/global.service';
import {ToastrService} from 'ngx-toastr';
import {OfficeService} from '../../../../services/office.service';
import {StaffService} from '../../../../services/staff.service';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import {IOffice} from '../../../../models/office';

@Component({
    templateUrl: 'office-edit.component.html'
})
export class OfficeEditComponent {

    officeFormGroup: FormGroup;
    activeBusiness: IBusiness;
    office: IOffice;
    member: IMember;
    uniqueId: string;
    selectedImage: string;

    // for db
    agents: any[] = [];
    managers: any[] = [];

    // for model
    agentList: IMember[];
    managersList: IMember[];

    @ViewChild('officeImage') officeImage: ElementRef;

    managerSearchFormGroup: FormGroup;
    agentSearchFormGroup: FormGroup;


    constructor(private _formBuilder: FormBuilder,
                private _router: Router,
                private _activatedRoute: ActivatedRoute,
                private _modal: NgbModal,
                private _business: BusinessService,
                private _staff: StaffService,
                private _toast: ToastrService,
                private _global: GlobalService,
                private _office: OfficeService) {
    }

    ngOnInit(): void {

        this._activatedRoute.data.subscribe((res: { office: IOffice, user: IMember }) => {
            this.office = res.office;
            this.member = res.user;
            this.uniqueId = this.office.uniqueId;
            this.activeBusiness = this.office.company;
            this.agents = this.office.agents;
            this.managers = this.office.managers;
            this.selectedImage = this.office.image;
            this.buildOfficeForm(this.office);
            if(this.member.role === 'admin') this.getAllManagers();
            this.getAllAgents();
            this.buildManagerSearchForm();
            this.buildAgentSearchForm();
        });

        this._business.currentBusiness.subscribe(res => {
            this.activeBusiness = res;
        });

        this.managerSearchFormGroup.valueChanges.subscribe(res => {
            if (!(res.managerQuery === '' || res.managerQuery === undefined)) {
                this.doSearchManagers(res.managerQuery);
            } else {
                this.getAllManagers();
            }
        });

        this.agentSearchFormGroup.valueChanges.subscribe(res => {
            if (!(res.agentQuery === '' || res.agentQuery === undefined)) {
                this.doSearchAgents(res.agentQuery);
            } else {
                this.getAllAgents();
            }
        });

    }

    openManagersModel(content: any): void {
        this._modal.open(content, {size: 'lg'}).result.then(res => console.log(res), err => console.log(err))
    }

    triggerUpload(): void {
        this.officeImage.nativeElement.click();
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    buildOfficeForm(office: IOffice): void {
        this.officeFormGroup = this._formBuilder.group({
            officeName: [office.officeName ? office.officeName : '', Validators.required],
            location: [office.location ? office.location : '', Validators.required],
            tags: [office.tags ? office.tags : ''],
            nameOfRoad: [office.nameOfRoad ? office.nameOfRoad : '', Validators.required],
            numberOfWay: [office.numberOfWay ? office.numberOfWay : '', Validators.required]
        });
    }

    getAllManagers(): void {
        let filters = {
            role: 'manager',
            company: this.activeBusiness._id
        };
        this._staff.getAll(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .map(res => res['data'])
            .subscribe(res => this.managersList = res, err => console.log(err));
    }

    buildManagerSearchForm(): void {
        this.managerSearchFormGroup = this._formBuilder.group({
            managerQuery: ['']
        })
    }

    addOrRemoveManager(member: any, index: number): void {
        let i = this.managers.findIndex(item => item._id === member._id);
        if (i === -1) {
            this.managers.push({_id: member._id, firstName: member.firstName, lastName: member.lastName});
            member['selected'] = true;
            this.managersList.splice(index, 1, member);
            this.addOrRemovePersonFromOffice(member._id, this.office._id, 'manager', 'add');
        } else {
            this.managers.splice(i, 1);
            if (member.selected) delete member.selected;
            this.managersList.splice(index, 1, member);
            this.addOrRemovePersonFromOffice(member._id, this.office._id, 'manager', 'add');
        }
    }

    removeManager(member: any, i: number): void {
        this.managers.splice(i, 1);
        let sManager = this.managersList.find((item: any) => item._id === member._id);
        delete sManager['selected'];
        let index = this.managersList.findIndex((item: any) => item._id === sManager._id);
        this.managersList.splice(index, 1, sManager);
        this.addOrRemovePersonFromOffice(member._id, this.office._id, 'manager', 'remove');
    }

    getAllAgents(): void {
        let filters = {
            role: 'agent',
            company: this.activeBusiness._id
        };
        this._staff.getAll(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .map(res => res['data'])
            .subscribe(res => this.agentList = res, err => console.log(err));
    }

    buildAgentSearchForm(): void {
        this.agentSearchFormGroup = this._formBuilder.group({
            agentQuery: ['']
        })
    }

    addOrRemoveAgent(member: any, index: number): void {
        let i = this.agents.findIndex(item => item._id === member._id);
        if (i === -1) {
            this.agents.push({_id: member._id, firstName: member.firstName, lastName: member.lastName});
            member['selected'] = true;
            this.agentList.splice(index, 1, member);
            this.addOrRemovePersonFromOffice(member._id, this.office._id, 'agent', 'add');
        } else {
            this.agents.splice(i, 1);
            if (member.selected) delete member.selected;
            this.agentList.splice(index, 1, member);
            this.addOrRemovePersonFromOffice(member._id, this.office._id, 'agent', 'remove');
        }
    }

    removeAgent(member: any, i: number): void {
        this.agents.splice(i, 1);
        let sAgent = this.agentList.find((item: any) => item._id === member._id);
        delete sAgent['selected'];
        let index = this.agentList.findIndex((item: any) => item._id === sAgent._id);
        this.agentList.splice(index, 1, sAgent);
        this.addOrRemovePersonFromOffice(member._id, this.office._id, 'agent', 'remove');
    }

    updateOffice(value: any): void {
        value.uniqueId = this.office.uniqueId;
        value.agents = this.agents ? this.agents = this.agents.map((item: any) => item._id) : [];
        value.managers = this.managers ? this.managers = this.managers.map((item: any) => item._id) : [];
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : '';
        let modifiedTags: any = [];
        if (value.tags) {
            for (let tag of value.tags) {
                typeof tag === 'string' ? modifiedTags.push(tag) : modifiedTags.push(tag['display'])
            }
        }
        value.tags = modifiedTags;
        value.company = this.activeBusiness._id;
        let office = this._office.update(value, this.office._id);
        office.subscribe(res => {
            this._toast.success('Office Updated Successfully', 'Success');
            this._router.navigate(['/', 'admin', 'business', 'offices'])
        }, err => {
            this._toast.error(err.message.toString(), 'Something went wrong!', {timeOut: 5000});
        });
    }

    doSearchManagers(query: string): void {
        let filters = {
            company: this.activeBusiness._id,
            role: 'manager',
            name: query
        };
        this._staff.searchByCompany(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe(res => this.managersList = res, err => console.log(err));
    }

    doSearchAgents(query: string): void {
        let filters = {
            company: this.activeBusiness._id,
            role: 'agent',
            name: query
        };
        this._staff.searchByCompany(filters)
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe(res => this.agentList = res, err => console.log(err));
    }

    trackByFn(index: number, item: any) {
        return index;
    }

    addOrRemovePersonFromOffice(personId: string, officeId: string, role: string, method: string): void {
        let body = {
            personId,
            officeId,
            role
        };
        this._office.addOrRemoveStaffFromOffice(body, method).subscribe(res => {
            if(method === 'add') this._toast.success(`Person Added! Office Updated.`, 'Success');
            if(method === 'remove') this._toast.success(`Person Removed! Office Updated.`, 'Success');
        }, err => {
            this._toast.success(err.message, 'Error')
        })
    }
}