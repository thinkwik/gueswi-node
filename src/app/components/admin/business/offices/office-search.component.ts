import {Component, OnInit} from '@angular/core';
import swal from 'sweetalert2';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OfficeService} from '../../../../services/office.service';
import {IOffice} from '../../../../models/office';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';
import {IMember} from '../../../../models/member';

@Component({
    templateUrl: 'office-search.component.html'
})
export class OfficeSearchComponent implements OnInit {

    offices: IOffice[];
    member: IMember;
    searchFormGroup: FormGroup;
    filters: any;

    constructor(private _formBuilder: FormBuilder,
                private _toast: ToastrService,
                private _activatedRoute: ActivatedRoute,
                private _office: OfficeService) {

    }

    ngOnInit(): void {
        this.buildSearchForm();
        this.getAllOffices();
        this._activatedRoute.data.subscribe((res:{user: IMember}) => {
           this.member = res.user;
        });
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.query === '' || res.query === undefined)) {
                this.doSearchOffice(res.query);
            } else {
                this.getAllOffices();
            }
        })
    }

    buildSearchForm() {
        this.searchFormGroup = this._formBuilder.group({
            query: ['']
        })
    }

    doSearchOffice(name: any): void {
        let query = {
            name,
            company: localStorage.getItem('_bid')
        };
        this._office.getOfficesByCompany('name', query)
            .subscribe(res => this.offices = res);
    }

    deleteOffice(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this Office?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let remove = this._office.delete(id);
            remove.subscribe(res => {
                this._toast.error('Office Deleted', 'Success');
                this.getAllOffices();
            }, err => {
                this._toast.error(err.message.toString(), 'Error', {timeOut: 5000});
            });
        }).catch(err => {
            this._toast.error(err.message.toString(), 'Error', {timeOut: 5000});
        })
    }

    getAllOffices() {
        this._office.getOfficesByCompany('id', localStorage.getItem('_bid')).subscribe(res => this.offices = res);
    }
}

