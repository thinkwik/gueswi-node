import {Component, OnInit} from '@angular/core';
import {IBusiness} from '../../../../models/business';
import {Observable} from 'rxjs/Observable';
import {IOffice} from '../../../../models/office';
import {GlobalService} from '../../../../services/global.service';
import {ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';
import {OfficeService} from '../../../../services/office.service';
import {IMember} from '../../../../models/member';
import {ToastrService} from 'ngx-toastr';

@Component({
    templateUrl: 'office-list.component.html'
})
export class OfficeListComponent implements OnInit {

    activeBusiness: IBusiness;
    user: IMember;
    offices: Observable<IOffice[]>;
    filterObject: any = {
        limit: GlobalService.paginatonOptions.limit,
        offset: GlobalService.paginatonOptions.offset
    };
    total: number;
    page: any;

    constructor(private _activatedRoute: ActivatedRoute,
                private _toast: ToastrService,
                private _office: OfficeService) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { business: IBusiness, user: IMember }) => {
            this.activeBusiness = res.business;
            this.user = res.user;
        });
        this.getPage(1);
    }

    getPage(page: number) {
        this.filterObject.offset = this.filterObject.limit * (page - 1);
        this.filterObject.company = this.activeBusiness._id;
        this.offices = this._office.getAll(this.filterObject)
            .do(res => {
                this.total = res['totalItems'];
                this.page = page;
            }).map(res => res['data'])
    }

    deleteOffice(id: string): void {
        swal({
            title: 'Are you sure, you want to delete this Office?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(() => {
            let list = this._office.getAll(this.filterObject);
            let remove = this._office.delete(id);
            remove.switchMap(() => list).subscribe(res => {
                this.getPage(this.page);
                this._toast.error('Office Deleted', 'Success');
            }, err => {
                this._toast.error(err.message.toString(), 'Error', {timeOut: 5000});
            });
        }).catch(err => {
            this._toast.error(err.message.toString(), 'Error', {timeOut: 5000});
        })
    }
}
