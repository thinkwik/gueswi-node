import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {GlobalService} from '../../../../services/global.service';
import {OfficeService} from '../../../../services/office.service';
import {StaffService} from '../../../../services/staff.service';
import {IBusiness} from '../../../../models/business';

@Component({
    templateUrl: 'agent-create.component.html'
})
export class AgentCreateComponent {

    @ViewChild('userImage') userImage: ElementRef;
    userFormGroup: FormGroup;
    searchFormGroup: FormGroup;
    selectedImage: string;
    activeBusiness: IBusiness;
    offices: any;

    assignedOffices: any[] = [];

    constructor(private _global: GlobalService,
                private _modal: NgbModal,
                private _router: Router,
                private _office: OfficeService,
                private _staff: StaffService,
                private _toast: ToastrService,
                private _activatedRoute: ActivatedRoute,
                private _formBuilder: FormBuilder) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { business: IBusiness }) => {
            this.activeBusiness = res.business;
            this.buildUserFormGroup();
            this.buildSearchForm();
            this.getAllOffices();
        });
        this.searchFormGroup.valueChanges.subscribe(res => {
            if (!(res.searchOfficeName === undefined || res.searchOfficeName === '')) {
                this.doSearchOffice(res.searchOfficeName)
            } else {
                this.getAllOffices();
            }
        });
    }

    buildUserFormGroup() {
        this.userFormGroup = this._formBuilder.group({
            telephone: ['', Validators.required],
            position: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            password: ['', Validators.required],
            email: ['', Validators.required],
            tags: [],
            address: ['']
        })
    }

    buildSearchForm() {
        this.searchFormGroup = this._formBuilder.group({
            searchOfficeName: ['']
        })
    }

    triggerUpload() {
        this.userImage.nativeElement.click();
    }

    openModel(content: any): void {
        this._modal.open(content, {size: 'lg'}).result.then(res => console.log(res), err => console.log(err));
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    createAgent(value: any): void {
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : '';
        value.tags = value.tags ? value.tags = value.tags.map((item: any) => item.display) : [];
        value.role = 'agent';
        value.company = this.activeBusiness._id;
        value.offices = this.assignedOffices ? this.assignedOffices = this.assignedOffices.map((item: any) => item.id) : [];
        let body = {memberRole: 'agent', companyId: this.activeBusiness._id};
        let assign = this._staff.assignCompanyToMember(body);
        let create = this._staff.create(value);
        assign.switchMap(() => create).subscribe(res => {
            this._router.navigate(['/', 'admin', 'business', 'agents', 'list']);
            this._toast.success('New Agent Created', 'Success!');
        }, err => {
            this._toast.error(err.message.toString(), 'Add Failed!', {timeOut: 5000});
        });


    }

    getAllOffices() {
        this._office.getOfficesByCompany('id', this.activeBusiness._id).subscribe(res => this.offices = res)
    }

    addOrRemoveOffice(office: any, index: number): void {
        let i = this.assignedOffices.findIndex(item => item.id === office._id);
        if (i === -1) {
            this.assignedOffices.push({id: office._id, name: office.officeName});
            office['selected'] = true;
            this.offices.splice(index, 1, office);
        } else {
            this.assignedOffices.splice(i, 1);
            if (office.selected) delete office.selected;
            this.offices.splice(index, 1, office);
        }
    }

    removeOffice(office: any, i: number) {
        this.assignedOffices.splice(i, 1);
        let sOffice = this.offices.find((item: any) => item._id === office.id);
        delete sOffice.selected;
        let index = this.offices.findIndex((item: any) => item._id === sOffice._id);
        this.offices.splice(index, 1, sOffice);
    }


    doSearchOffice(name: any): void {
        let query = {
            name,
            company: this.activeBusiness._id
        };
        this._office.getOfficesByCompany('name', query).subscribe(res => this.offices = res);
    }

    trackByFn(index: number, item: any) {
        return index;
    }
}