import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


/* COMPONENTS */
import {BusinessHomeComponent} from './business-home/business-home.component';
import {CompanyComponent} from './company/company.component';
import {AgentListComponent} from './agents/agent-list.component';
import {AgentSearchComponent} from './agents/agent-search.component';
import {AgentCreateComponent} from './agents/agent-create.component';
import {AgentEditComponent} from './agents/agent-edit.component';
import {ManagerListComponent} from './managers/manager-list.component';
import {ManagerSearchComponent} from './managers/manager-search.component';
import {ManagerCreateComponent} from './managers/manager-create.component';
import {ManagerEditComponent} from './managers/manager-edit.component';
import {OfficeCreateComponent} from './offices/office-create.component';
import {OfficeEditComponent} from './offices/office-edit.component';
import {OfficeListComponent} from './offices/office-list.component';
import {OfficeSearchComponent} from './offices/office-search.component';
import {RatingListComponent} from './ratings/rating-list.component';
import {RatingCreateComponent} from './ratings/rating-create.component';
import {RatingEditComponent} from './ratings/rating-edit.component';
import {RecruitmentIndexComponent} from './recruitment/recruitment.index.component';
import {WhatsappIndexComponent} from './whatsapp/whatsapp.index.component';

/* SERVICE */
import {BusinessResolver} from '../../../services/business.service';
import {LoggedInUserResolver, UserResolver} from '../../../services/staff.service';
import {OfficeResolver} from '../../../services/office.service';

/* ROUTES */

const routes: Routes = [
    {
        path: '',
        component: BusinessHomeComponent,
        resolve: {user: LoggedInUserResolver, business: BusinessResolver},
        children: [
            {path: '', redirectTo: 'company-details', pathMatch: 'full'},
            {path: 'company-details', component: CompanyComponent},
            {
                path: 'agents',
                children: [
                    {path: '', redirectTo: 'list', pathMatch: 'full'},
                    {path: 'list', component: AgentListComponent, resolve: {business: BusinessResolver}},
                    {path: 'search', component: AgentSearchComponent},
                    {path: 'create', component: AgentCreateComponent, resolve: {business: BusinessResolver}},
                    {path: 'edit/:id', component: AgentEditComponent, resolve: {user: UserResolver}}
                ]
            },
            {
                path: 'managers',
                children: [
                    {path: '', redirectTo: 'list', pathMatch: 'full'},
                    {path: 'list', component: ManagerListComponent, resolve: {business: BusinessResolver}},
                    {path: 'search', component: ManagerSearchComponent},
                    {path: 'create', component: ManagerCreateComponent, resolve: {business: BusinessResolver}},
                    {path: 'edit/:id', component: ManagerEditComponent, resolve: {user: UserResolver}}
                ]
            },
            {
                path: 'offices',
                children: [
                    {path: '', redirectTo: 'list', pathMatch: 'full'},
                    {path: 'list', component: OfficeListComponent, resolve: {business: BusinessResolver, user: LoggedInUserResolver}},
                    {path: 'search', component: OfficeSearchComponent, resolve: {user: LoggedInUserResolver}},
                    {path: 'create', component: OfficeCreateComponent, resolve: {business: BusinessResolver}},
                    {path: 'edit/:id', component: OfficeEditComponent, resolve: {office: OfficeResolver, user: LoggedInUserResolver}}
                ]
            },
            {
                path: 'ratings',
                children: [
                    {path: '', redirectTo: 'list', pathMatch: 'full'},
                    {path: 'list', component: RatingListComponent},
                    {path: 'create', component: RatingCreateComponent},
                    {path: 'edit/:id', component: RatingEditComponent}
                ]
            },
            {path: 'recruitment', component: RecruitmentIndexComponent},
            {path: 'whatsapp', component: WhatsappIndexComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class BusinessRoutingModule {

}