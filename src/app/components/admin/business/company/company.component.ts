import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IBusiness} from '../../../../models/business';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../../services/global.service';
import {BusinessService} from '../../../../services/business.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    templateUrl: 'company.component.html'
})
export class CompanyComponent implements OnInit {

    business: IBusiness;
    businessFormGroup: FormGroup;
    selectedImage: string;
    @ViewChild('businessImage') businessImage: ElementRef;

    constructor(private _activatedRoute: ActivatedRoute,
                private _global: GlobalService,
                private _toast: ToastrService,
                private _business: BusinessService,
                private _formBuilder: FormBuilder) {

    }

    ngOnInit(): void {
        this._activatedRoute.parent.data.subscribe((res: { business: IBusiness }) => {
            if (res.business) {
                this.business = res.business;
                this._business.currentBusiness.next(this.business);
                this.selectedImage = res.business.image;
                this.buildBusinessForm(this.business);
            }
        })

    }

    buildBusinessForm(business: IBusiness): void {
        this.businessFormGroup = this._formBuilder.group({
            name: [business ? business.name : '', Validators.required],
            tradeName: [business ? business.tradeName : '', Validators.required],
            location: [business ? business.location : '', Validators.required],
            phone: [business ? business.phone : '', Validators.required],
            nameOfRoad: [business ? business.nameOfRoad : '', Validators.required],
            CIF: [business ? business.CIF : '', Validators.required],
            numberOfWay: [business ? business.numberOfWay : '', Validators.required],
            contactPerson: [business ? business.contactPerson : '', Validators.required],
        })
    }

    triggerUpload() {
        this.businessImage.nativeElement.click();
    }

    changeFile(event: any): void {
        let file: File = event.target.files[0];
        let formData: FormData = new FormData();
        formData.append('image', file, file.name);
        let upload = this._global.upload(formData);
        upload.subscribe(res => {
            this.selectedImage = res.image;
        }, err => {
            console.log(err);
        })
    }

    updateCompany(value: any): void {
        value.image = this.selectedImage ? this._global.getBaseName(this.selectedImage) : this.business.image;
        let obs$ = this._business.updateForAdmin(value, this.business._id);
        obs$.subscribe(res => {
            this.business = res;
            this._business.currentBusiness.next(res);
            this.selectedImage = this.business.image;
            this.buildBusinessForm(this.business);
            this._toast.success('Business details updated', 'Success');
        }, err => {
            this._toast.error(err.message.toString(), 'Error');
        })
    }


}
