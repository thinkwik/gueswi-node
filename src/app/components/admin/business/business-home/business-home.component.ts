import {Component, OnInit} from '@angular/core';
import {StaffService} from '../../../../services/staff.service';
import {IMember} from '../../../../models/member';
import {ActivatedRoute, Router} from '@angular/router';
import {BusinessService} from '../../../../services/business.service';
import {IBusiness} from '../../../../models/business';

@Component({
    templateUrl: 'business-home.component.html'
})
export class BusinessHomeComponent implements OnInit {

    user: IMember;
    business: IBusiness;
    menu: any[] = [];

    admin: any[] = [
        {
            moduleName: 'Business',
            link: ['/', 'admin', 'business', 'company-details']
        },
        {
            moduleName: 'Offices',
            link: ['/', 'admin', 'business', 'offices']
        },
        {
            moduleName: 'Managers',
            link: ['/', 'admin', 'business', 'managers']
        },
        {
            moduleName: 'Agents',
            link: ['/', 'admin', 'business', 'agents']
        },
        {
            moduleName: 'Ratings',
            link: ['/', 'admin', 'business', 'ratings']
        },
        {
            moduleName: 'Hiring',
            link: ['/', 'admin', 'business', 'recruitment']
        },
        {
            moduleName: 'WhatsApp',
            link: ['/', 'admin', 'business', 'whatsapp']
        },
    ];
    manager: any[] = [
        {
            moduleName: 'Business',
            link: ['/', 'admin', 'business', 'company-details']
        },
        {
            moduleName: 'Offices',
            link: ['/', 'admin', 'business', 'offices']
        },
        {
            moduleName: 'Agents',
            link: ['/', 'admin', 'business', 'agents']
        },
        {
            moduleName: 'Ratings',
            link: ['/', 'admin', 'business', 'ratings']
        },
        {
            moduleName: 'Hiring',
            link: ['/', 'admin', 'business', 'recruitment']
        },
        {
            moduleName: 'WhatsApp',
            link: ['/', 'admin', 'business', 'whatsapp']
        },
    ];
    agent: any[] = [
        {
            moduleName: 'Ratings',
            link: ['/', 'admin', 'business', 'ratings']
        },
        {
            moduleName: 'Hiring',
            link: ['/', 'admin', 'business', 'recruitment']
        },
        {
            moduleName: 'WhatsApp',
            link: ['/', 'admin', 'business', 'whatsapp']
        },
    ];

    constructor(private _staff: StaffService,
                private _router: Router,
                private _business: BusinessService,
                private _activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this._activatedRoute.data.subscribe((res: { user: IMember, business: IBusiness }) => {
            this.user = res.user;
            this.buildUserMenu(this.user.role);
            this.business = res.business;
            this._staff.user.next(res.user);
            this._business.currentBusiness.next(res.business);
        });
        this._business.currentBusiness.subscribe(res => this.business = res)
    }

    logout(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('_bid');
        this._router.navigate(['/']);
    }


    buildUserMenu(role: string) {
        switch (role) {
            case 'admin':
                this.menu = this.admin;
                break;
            case 'manager':
                this.menu = this.manager;
                break;
            case 'agent':
                this.menu = this.agent;
                break;
        }

    }
}
