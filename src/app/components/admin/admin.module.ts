import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from './admin.routing.module'
import {AdminHomeComponent} from './dashboard/admin-home.component';
import {OverviewComponent} from './overview/overview.component';
import {SharedModule} from '../shared/shared.module';
/* SERVICES */
import {BusinessAuth, BusinessService} from '../../services/business.service';
import {UserAuth} from '../../services/staff.service';
import {ManagerService} from '../../services/manager.service';
import {ProfileComponent} from './profile/profile.component';


@NgModule({
    imports: [AdminRoutingModule, CommonModule, SharedModule],
    declarations: [ProfileComponent, AdminHomeComponent, OverviewComponent],
    providers: [BusinessService, ManagerService, UserAuth, BusinessAuth]
})

export class AdminModule {

}
