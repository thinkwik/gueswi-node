import {IMember} from './member';
import {IBusiness} from './business';

export interface IOffice {
    uniqueId: string;
    officeName: string;
    image: string;
    location: string;
    nameOfRoad: string;
    numberOfWay: string;
    managers: Array<IMember>;
    agents: Array<IMember>;
    company: IBusiness;
    tags: Array<any>
    isDeleted: boolean;
    id?: string;
    _id?: string;
}
