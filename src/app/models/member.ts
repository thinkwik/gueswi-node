import {IBusiness} from './business';
import {IOffice} from './office';

export class IMember {
    email: string;
    password: string;
    role: string;
    firstName: string;
    lastName: string;
    image: string;
    address: string;
    telephone: string;
    position: string;
    tags: Array<any>;
    company: IBusiness;
    offices: Array<IOffice>;
    id: string;
    _id: string;
}

