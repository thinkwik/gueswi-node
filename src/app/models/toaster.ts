export class ToasterConfig {
    public static timeOut = 3000;
    public static positionClass = 'toast-bottom-right';
    public static tapToDismiss = true;
    public static preventDuplicates = true;
}
