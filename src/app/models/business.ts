export interface IBusiness {
    name: string;
    tradeName: string;
    image: string;
    location: string;
    lat: number;
    long: number;
    nameOfRoad: string;
    numberOfWay: string;
    phone: string;
    CIF: string;
    contactPerson: string;
    createdBy: string;
    staff: Array<any>;
    offices: Array<any>;
    isDeleted: boolean;
    office: number;
    agent: number;
    manager: number;
    contractStartDate: string;
    contractEndDate: string;
    password: string;
    email: string;
    salesPricePerOffice: number;
    id?: string;
    _id?: string;
}
