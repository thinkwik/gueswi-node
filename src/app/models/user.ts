export interface IUser {
    role: string;
    firstName: string;
    lastName: string;
    email: string;
    _id?: string;
}
