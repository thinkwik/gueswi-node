// sweetalert2
import './assets/scss/sweetalert2.scss';
import './assets/js/sweetalert2.js';

// for toaster
import '../node_modules/ngx-toastr/toastr.css';

// core bootstrap
import './assets/bootstrap/bootstrap.scss';

// project styles
import './assets/scss/styles.scss';
