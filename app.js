const html = __dirname + '/dist/';
const env = process.NODE_ENV ? process.NODE_ENV : "development";

const express = require('express');
const bodyParser = require('body-parser');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();
const app = express();
const mongoose = require('mongoose');


//global configuration file
global.CONFIG = require('./api/config')[env];
global.ROOT_DIRECTORY = __dirname;

//express configuration
app.use(multipartMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(html));
app.listen(CONFIG.PORT);

//connecting to mongodb
mongoose.connect(CONFIG.MONGODB.URL, {useMongoClient: true});

//global routes handler
require('./api/routes')(app, express);

//opening routes
app.use('/uploads', express.static('src/uploads'));


//resending the same index file on every request
app.get('/*', function (req, res) {
    res.sendFile(html + '/index.html');
});
