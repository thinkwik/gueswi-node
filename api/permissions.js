module.exports = {
    SUPER_USER: {
        CREATE: {
            BUSINESS: {hasAccess: true},
            OFFICE: {hasAccess: true},
            RATING: {hasAccess: true},
            AGENTS: {hasAccess: true},
            MANAGERS: {hasAccess: true},
            STAFF: {hasAccess: true},
            SUPER_USER: {hasAccess: true},
            SUPER_ADMIN: {hasAccess: true}
        },
        MODIFY: {
            BUSINESS: {hasAccess: true},
            OFFICE: {hasAccess: true},
            RATING: {hasAccess: true},
            AGENTS: {hasAccess: true},
            STAFF: {hasAccess: true},
            MANAGERS: {hasAccess: true},
            SUPER_USER: {hasAccess: true},
            SUPER_ADMIN: {hasAccess: true}
        },
        DELETE: {
            BUSINESS: {hasAccess: true},
            OFFICE: {hasAccess: true},
            RATING: {hasAccess: true},
            AGENTS: {hasAccess: true},
            STAFF: {hasAccess: true},
            MANAGERS: {hasAccess: true},
            SUPER_USER: {hasAccess: true},
            SUPER_ADMIN: {hasAccess: true}
        }
    },
    ADMIN: {
        CREATE: {
            BUSINESS: {hasAccess: false},
            OFFICE: {hasAccess: true},
            RATING: {hasAccess: true},
            AGENTS: {hasAccess: true},
            MANAGERS: {hasAccess: true},
            STAFF: {hasAccess: true},
            SUPER_USER: {hasAccess: false},
            SUPER_ADMIN: {hasAccess: false}
        },
        MODIFY: {
            BUSINESS: {hasAccess: true},
            OFFICE: {hasAccess: true},
            RATING: {hasAccess: true},
            AGENTS: {hasAccess: true},
            MANAGERS: {hasAccess: true},
            STAFF: {hasAccess: true},
            SUPER_USER: {hasAccess: false},
            SUPER_ADMIN: {hasAccess: false}
        },
        DELETE: {
            BUSINESS: {hasAccess: false},
            OFFICE: {hasAccess: true},
            RATING: {hasAccess: true},
            AGENTS: {hasAccess: true},
            MANAGERS: {hasAccess: true},
            STAFF: {hasAccess: true},
            SUPER_USER: {hasAccess: false},
            SUPER_ADMIN: {hasAccess: false}
        }
    },
    MANAGER: {
      CREATE: {
        BUSINESS: {hasAccess: false},
        OFFICE: {hasAccess: false},
        RATING: {hasAccess: true},
        AGENTS: {hasAccess: true},
        MANAGERS: {hasAccess: false},
        STAFF: {hasAccess: true},
        SUPER_USER: {hasAccess: false},
        SUPER_ADMIN: {hasAccess: false}
      },
      MODIFY: {
        BUSINESS: {hasAccess: true},
        OFFICE: {hasAccess: true},
        RATING: {hasAccess: true},
        AGENTS: {hasAccess: true},
        MANAGERS: {hasAccess: false},
        STAFF: {hasAccess: true},
        SUPER_USER: {hasAccess: false},
        SUPER_ADMIN: {hasAccess: false}
      },
      DELETE: {
        BUSINESS: {hasAccess: false},
        OFFICE: {hasAccess: false},
        RATING: {hasAccess: true},
        AGENTS: {hasAccess: true},
        MANAGERS: {hasAccess: false},
        STAFF: {hasAccess: true},
        SUPER_USER: {hasAccess: false},
        SUPER_ADMIN: {hasAccess: false}
      }
    }
};