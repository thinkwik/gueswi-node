const express = require('express'),
      router = express(),
      auth = require('../auth'),
      Staff = require('../controllers/server.staff.controller');

/* FOR DEV PURPOSES */

router.post('/register', Staff.create);

/* USED FOR APP */

router.get('/me', auth.checkAuthentication, Staff.me);
router.post('/login', Staff.login);
router.post('/create', auth.checkAccess('CREATE.STAFF'), Staff.create);
router.get('/get/:id', auth.checkAuthentication, Staff.get);
router.post('/getAll', auth.checkAuthentication, Staff.getAll);
router.put('/update/:id', auth.checkAccess('MODIFY.STAFF'), Staff.update);
router.delete('/delete/:id', auth.checkAccess('DELETE.STAFF'), Staff.delete);
router.post('/checkAssignToCompany', auth.checkAuthentication, Staff.assignCompanyToMember);

/* SEARCHING */
router.get('/search/:query', auth.checkAuthentication, Staff.search); // searches everywhere in staff (for super_user)
router.post('/searchByCompany',  auth.checkAuthentication, Staff.searchByCompany); //search particular role in company

/* UPDATE STAFF */

router.post('/add/office', auth.checkAccess('MODIFY.STAFF'), Staff.addOfficeToMembers);
router.post('/delete/office', auth.checkAccess('DELETE.STAFF'), Staff.removeOfficeFromMembers);


module.exports = router;