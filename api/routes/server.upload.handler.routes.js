const express = require('express'),
    router = express(),
    helper = require('../helper'),
    auth = require('../auth');

router.post('/', auth.checkAuthentication, helper.uploadImage);

module.exports = router;
