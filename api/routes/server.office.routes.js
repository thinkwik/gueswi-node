const express = require('express'),
      router = express(),
      auth = require('../auth'),
      Office = require('../controllers/server.office.controller');

router.post('/getAll', auth.checkAuthentication, Office.getAll);
router.get('/get/:id', auth.checkAuthentication, Office.get);
router.post('/create', auth.checkAccess('CREATE.OFFICE'), Office.create);
router.put('/update/:id', auth.checkAccess('MODIFY.OFFICE'), Office.update);
router.delete('/delete/:id', auth.checkAccess('DELETE.OFFICE'), Office.delete);

/* FOR SEARCHING */
router.get('/getDataByCompanyId/:companyId', auth.checkAuthentication, Office.getOfficesByBusinessId);
router.get('/getDataByCompanyName/:company/:name', auth.checkAuthentication, Office.searchOfficeByCompany);

/* For UPDATING STAFF (agents, managers etc...) */

router.post('/add/staff', auth.checkAccess('MODIFY.STAFF'), Office.addStaffToOffice);
router.post('/delete/staff', auth.checkAccess('DELETE.STAFF'), Office.removeStaffFromOffice);

module.exports = router;