const express = require('express'),
      router = express(),
      auth = require('../auth'),
      helper = require('../helper'),
      Business = require('../controllers/server.business.controller');

router.post('/create', auth.checkAccess('CREATE.BUSINESS'), Business.create);
router.post('/getAll', auth.checkAuthentication, Business.getAll);
router.get('/get/:id', auth.checkAuthentication, Business.get);
router.put('/update/:id', auth.checkAccess('MODIFY.BUSINESS'), Business.update);
router.put('/updateForAdmin/:id', auth.checkAccess('MODIFY.BUSINESS'), Business.updateBusinessForAdmin);
router.delete('/delete/:id', auth.checkAccess('DELETE.BUSINESS'), Business.delete);
router.get('/search/:query', auth.checkAuthentication, Business.search);

module.exports = router;