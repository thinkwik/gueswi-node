const mongoose = require('bluebird').promisifyAll(require('mongoose')),
    Schema = mongoose.Schema,
    crypto = require('crypto-js'),
    autopopulate = require('mongoose-autopopulate'),
    pagination = require('mongoose-paginate'),
    timestamps = require('mongoose-simpletimestamps').SimpleTimestamps;

getLogoUrl = (val) => {
    if (!val) return val;
    return (`${global.CONFIG.IMAGE_PATH}/${val}`);
};

getPassword = (password) => {
    if (password) {
        let bytes = crypto.AES.decrypt(password.toString(), global.CONFIG.CRYPTO.KEY);
        return bytes.toString(crypto.enc.Utf8);
    }
};

let BusinessSchema = new Schema({

    name: {type: String, default: ''},
    tradeName: {type: String, default: ''},
    image: {type: String, get: getLogoUrl, default: ''},
    location: {type: String, default: ''},
    lat: {type: Number, default: 0},
    long: {type: Number, default: 0},
    nameOfRoad: {type: String, default: ''},
    numberOfWay: {type: String, default: ''},
    phone: {type: String, default: ''},
    CIF: {type: String, default: ''},
    contactPerson: {type: String, default: ''},

    office: {type: Number, default: 0},
    agent: {type: Number, default: 0},
    manager: {type: Number, default: 0},

    createdBy: {type: mongoose.Schema.Types.ObjectId},

    contractStartDate: {type: String, default: ''},
    contractEndDate: {type: String, default: ''},
    salesPricePerOffice: {type: String, default: ''},

    staff: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Staff',
        autopopulate: {select: "firstName lastName role"},
    }],

    offices: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Office',
        autopopulate: {select: "officeName"},
    }],

    password: {type: String, get: getPassword, default: ''},
    email: {type: String, default: ''},
    isDeleted: {type: Boolean, default: false}
});

BusinessSchema.plugin(autopopulate);
BusinessSchema.plugin(timestamps);
BusinessSchema.plugin(pagination);
BusinessSchema.index({name: 1});
BusinessSchema.set('toJSON', {getters: true});

BusinessSchema.methods.setPassword = (password) => {
    return crypto.AES.encrypt(password, global.CONFIG.CRYPTO.KEY);
};

BusinessSchema.methods.retrivePassword = (password) => {
    let bytes = crypto.AES.decrypt(password.toString(), global.CONFIG.CRYPTO.KEY);
    return bytes.toString(crypto.enc.Utf8);
};

module.exports = mongoose.model('Business', BusinessSchema);