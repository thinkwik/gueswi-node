const mongoose = require('bluebird').promisifyAll(require('mongoose')),
    Schema = mongoose.Schema,
    autopopulate = require('mongoose-autopopulate'),
    pagination = require('mongoose-paginate'),
    timestamps = require('mongoose-simpletimestamps').SimpleTimestamps;

getLogoUrl = (val) => {
    if (!val) return val;
    return (`${global.CONFIG.IMAGE_PATH}/${val}`);
};

let officeSchema = new Schema({

    officeName: {type: String, default: ''},
    image: {type: String, get: getLogoUrl, default: ''},
    createdBy: {type: mongoose.Schema.Types.ObjectId},
    location: {type: String, default: ''},
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business',
        autopopulate: {select: "name image"}
    },
    nameOfRoad: {type: String, default: ''},
    numberOfWay: {type: String, default: ''},
    uniqueId: {type: String, default: ''},

    agents: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Staff',
        autopopulate: {select: "firstName lastName"},
    }],

    managers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Staff',
        autopopulate: {select: "firstName lastName"},
    }],

    tags: [{type: String, default: []}],

    isDeleted: {type: Boolean, default: false}

});

officeSchema.plugin(timestamps);
officeSchema.plugin(autopopulate);
officeSchema.plugin(pagination);
officeSchema.index({officeName: 1});
officeSchema.index({company: 1});
officeSchema.set('toJSON', {getters: true});

module.exports = mongoose.model('Office', officeSchema);
