const crypto = require('crypto-js'),
    jwt = require('jsonwebtoken'),
    mongoose = require('bluebird').promisifyAll(require('mongoose')),
    Schema = mongoose.Schema,
    autopopulate = require('mongoose-autopopulate'),
    pagination = require('mongoose-paginate'),
    timestamps = require('mongoose-simpletimestamps').SimpleTimestamps;

getImageUrl = (val) => {
    if (!val) return val;
    return (`${global.CONFIG.IMAGE_PATH}/${val}`);
};

getPassword = (password) => {
    if (password) {
        let bytes = crypto.AES.decrypt(password.toString(), global.CONFIG.CRYPTO.KEY);
        return bytes.toString(crypto.enc.Utf8);
    }
};

let StaffModel = new Schema({

    email: {type: String, lowercase: true, required: [true, 'No Email Provided'], default: ''},
    password: {type: String, get: getPassword, required: [true, 'No Password Provided'], default: ''},

    role: {type: String, enum: ['super_user', 'manager', 'agent', 'admin']},

    firstName: {type: String, required: [true, 'No First Name Provided']},
    lastName: {type: String, required: [true, 'No last Name Provided']},

    image: {type: String, default: '', get: getImageUrl},
    address: {type: String, default: ''},
    telephone: {type: String, default: ''},
    position: {type: String, default: ''},
    tags: [{type: String, default: ''}],

    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business',
        autopopulate: {select: "name image"}
    },

    offices: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Office',
        autopopulate: {select: "officeName"}
    }],

    isDeleted: {type: Boolean, default: false}
});

StaffModel.plugin(autopopulate);
StaffModel.plugin(timestamps);
StaffModel.plugin(pagination);
StaffModel.index({firstName: 1});
StaffModel.index({lastName: 1});
StaffModel.set('toJSON', {getters: true});

StaffModel.methods.setPassword = (password) => {
    return crypto.AES.encrypt(password, global.CONFIG.CRYPTO.KEY);
};

StaffModel.methods.retrivePassword = (password) => {
    let bytes = crypto.AES.decrypt(password, global.CONFIG.CRYPTO.KEY);
    return bytes.toString(crypto.enc.Utf8);
};

StaffModel.methods.getToken = (user) => {
    return jwt.sign({user: user}, global.CONFIG.AUTH.SECRET, {expiresIn: global.CONFIG.TOKEN.EXPIRY});
};

module.exports = mongoose.model('Staff', StaffModel);