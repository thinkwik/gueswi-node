const jwt = require('jsonwebtoken');
const permissions = require('./permissions');


exports.checkAuthentication = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers.authorization;
    if (token) {
        jwt.verify(token, global.CONFIG.AUTH.SECRET, (err, decoded) => {
            if (err) {
                return res.status(401).json({message: 'You are not authorized'})
            } else {
                req.decoded = decoded.user;
                next();
            }
        })
    } else {
        return res.status(401).json({message: 'You are not authorized'});
    }
};

exports.checkAccess = (action) => {
    return (req, res, next) => {
        let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers.authorization;
        if(token){
            jwt.verify(token, global.CONFIG.AUTH.SECRET, (err, decoded) => {
                if (err) {
                    return res.status(401).json({message: 'You are not authorized'})
                } else {
                    decoded.user.role = decoded.user.role.toUpperCase();
                    req.decoded = decoded.user;
                    if(decoded.user.role === 'SUPER_USER'){
                        return next();
                    } else {
                        let permission = permissions[decoded.user.role];
                        let [operation, module] = action.split('.');
                        let op = permission[operation];
                        if(op[module].hasAccess) {
                            return next();
                        } else {
                            return res.status(401).json({message: 'You don\'t have access to perform this operation'})
                        }
                    }
                }
            })
        } else{
            return res.status(401).json({message: 'You are not authorized'});
        }
    }
};