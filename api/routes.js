module.exports = (app, express) => {
    app.use('/api/staff', require('./routes/server.staff.routes'));
    app.use('/api/business', require('./routes/server.business.routes'));
    app.use('/api/office', require('./routes/server.office.routes'));
    app.use('/api/upload', require('./routes/server.upload.handler.routes'));
};
