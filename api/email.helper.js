const nodemailer = require('nodemailer');
const EmailTemplate = require('email-templates').EmailTemplate;
const path = require('path');

exports.send = (msgInfo) => {
    let transporter = nodemailer.createTransport({
        host: global.CONFIG.EMAIL.HOST,
        port: 465,
        secure: true,
        auth: {
            user: global.CONFIG.EMAIL.USER,
            pass: global.CONFIG.EMAIL.PASSWORD
        }
    });

    let message = {
        text: msgInfo.message,
        from: `The Developer <${global.CONFIG.EMAIL.USER}>`,
        to: msgInfo.to,
        subject: msgInfo.subject,
        html: msgInfo.html

    };

    transporter.sendMail(message, (err, res) => {
        if (err) console.error('something went wrong while sending mail.');
        else console.info('mail sent successfully!');
    });

};


exports.sendPassword = (data) => {
    let templateDir = path.join(ROOT_DIRECTORY, 'src', 'assets', 'email-templates', 'password-info');
    let policy = new EmailTemplate(templateDir);
    policy.render({data}, (err, email) => {
        if (err) {
            console.error('something went wrong while generating mail.');
        }
        else {
            let message = {
                message: 'Thank you for registering with us. Here is your password for Gueswi',
                html: email.html,
                to: data.email,
                subject: 'Thank you for registering with us. Here is your password for Gueswi'
            };
            exports.send(message);
        }
    });
};