const _ = require('lodash'),
    fs = require('fs'),
    path = require('path');

exports.handleResponse = (res, data) => {
  if (_.isNull(data) || _.isUndefined(data) || _.isEmpty(data)) {
    res.status(404);
    res.json({message: 'No Records Found!'});
  } else {
    res.status(200);
    res.json(data);
  }
};

exports.handleError = (res, err) => {
  if (err.name === 'ValidationError') {
    res.status(422);
    res.json({message: err.message});
  } else {
    res.status(500);
    res.json({message: err.message});
  }
};

exports.getUniqueId = (prefix) => {
  let uniqueId = prefix ? prefix + Math.floor(Date.now()) : Math.floor(Date.now()) + '';
  return uniqueId.toString().toUpperCase();
};

exports.basename = (path) => {
  return path.replace(/\\/g, '/').replace(/.*\//, '');
};

exports.uploadImage = (req, res) => {
  let image = !_.isEmpty(req.files) ? exports.uploadFile(req.files.image) : '';
  res.status(200).json({image: `${CONFIG.IMAGE_PATH}/${image}`});
};

exports.uploadFile = (image) => {
  if (!image.originalFilename.match(/\.(jpg|jpeg|png|gif)$/)) {
    return res.status(422).json({message: 'This is not an appropriate file type'});
  } else {
    let ext = path.extname(image.originalFilename);
    let imageName = exports.getUniqueId('IMG-');
    let file = ROOT_DIRECTORY + '/src/uploads/' + imageName + ext;
    fs.rename(image.path, file, (err) => {
      if (err) console.error(err);
    });
    return exports.basename(file);
  }
};


