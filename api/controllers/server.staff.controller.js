const Staff = require('../models/server.staff.model'),
    Business = require('../models/server.business.model'),
    Office = require('../models/server.office.model'),
    helper = require('../helper'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    emailHelper = require('../email.helper');

exports.create = (req, res) => {
  let staff = new Staff(req.body);
  let password = req.body.password;
  staff.password = staff.setPassword(req.body.password);
  Staff.findOneAsync({isDeleted: false, email: req.body.email})
      .then(doc => {
        if (doc === null) {
          Staff.createAsync(staff)
              .then(createdMember => {
                Business.findOneAndUpdateAsync({_id: createdMember.company}, {$push: {'staff': createdMember._id}}, {new: true})
                    .then(business => {
                      if (createdMember.role && createdMember.role === 'manager') {
                        if (createdMember.offices.length > 0) {
                          async.each(createdMember.offices, (office, callback) => {
                            Office.findOneAndUpdateAsync({_id: office}, {$push: {'managers': createdMember._id}})
                                .then(() => {
                                  callback();
                                })
                                .catch(err => {
                                  return helper.handleError(res, err);
                                })
                          }, (err) => {
                            if (err) return helper.handleError(err);
                          });
                        }
                      }
                      if (createdMember.role && createdMember.role === 'agent') {
                        if (createdMember.offices.length > 0) {
                          async.each(createdMember.offices, (office, callback) => {
                            Office.findOneAndUpdateAsync({_id: office}, {$push: {'agents': createdMember._id}})
                                .then(() => {
                                  callback();
                                })
                                .catch(err => {
                                  return helper.handleError(res, err);
                                })
                          }, (err) => {
                            if (err) return helper.handleError(err);
                          });
                        }
                      }
                      let data = {
                        email: createdMember.email,
                        firstName: createdMember.firstName,
                        lastName: createdMember.lastName,
                        password: password
                      };
                      emailHelper.sendPassword(data);
                      helper.handleResponse(res, createdMember);
                    })
                    .catch(err => helper.handleError(res, err));
              })
              .catch(err => helper.handleError(res, err));
        } else {
          res.status(409).json({message: 'User Already Exists.'});
        }
      }).catch(err => helper.handleError(res, err));
};

exports.login = (req, res) => {
  Staff.findOneAsync({email: req.body.email})
      .then(doc => {
        if (doc === null) {
          return res.status(404).json({message: 'User not found'});
        } else {
          let staff = new Staff(doc);
          if (staff.role === 'super_user') {
            returnUserWithData(staff);
          } else {
            Business.findOneAsync({_id: staff.company})
                .then(company => {
                  if (moment() > moment(new Date(company.contractEndDate))) {
                    return res.status(412).json({message: 'Your contract has been expired, please contact Gueswi Administrator for renewal of the contract.'});
                  } else {
                    returnUserWithData(staff);
                  }
                })
                .catch(err => helper.handleError(res, err))
          }
        }
      }).catch(err => helper.handleError(res, err));

  function returnUserWithData(staff) {
    if (staff.password === req.body.password) {
      let userData = {
        userId: staff._id,
        email: staff.email,
        role: staff.role,
        timeStamp: new Date()
      };
      let token = staff.getToken(userData);
      let response = {};
      response._id = staff._id;
      response.email = staff.email;
      response.role = staff.role;
      response.firstName = staff.firstName;
      response.lastName = staff.lastName;
      response.image = staff.image;
      response.address = staff.address;
      response.telephone = staff.position;
      response.tags = staff.tags;
      response.company = staff.company;
      response.offices = staff.offices;
      res.status(200).json({data: response, access_token: token});
    } else {
      return res.status(401).json({message: 'Password or Emails is not valid'});
    }
  }
};

exports.me = (req, res) => {
  Staff.findOneAsync({_id: req.decoded.userId})
      .then(doc => {
        let response = {};
        response._id = doc._id;
        response.email = doc.email;
        response.role = doc.role;
        response.firstName = doc.firstName;
        response.lastName = doc.lastName;
        response.image = doc.image;
        response.address = doc.address;
        response.telephone = doc.position;
        response.tags = doc.tags;
        response.company = doc.company;
        response.offices = doc.offices;
        res.status(200).json(response);
      })
      .catch(err => helper.handleError(res, err))
};

exports.get = (req, res) => {
  Staff.findOneAsync({_id: req.params.id})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleError(res, err))
};

exports.getAll = (req, res) => {
  let filters = {};
  filters.isDeleted = false;
  if (!req.body.offset) req.body.offset = 0;
  if (!req.body.limit) req.body.limit = 10;
  if (req.body.company) filters.company = req.body.company;
  if (req.body.role) filters.role = req.body.role;
  Staff.paginate(filters, {
    select: {firstName: 1, lastName: 1, _id: 1, role: 1, offices: 1},
    offset: parseInt(req.body.offset),
    limit: parseInt(req.body.limit)
  })
      .then(result => helper.handleResponse(res, {data: result.docs, totalItems: result.total}))
      .catch(err => helper.handleError(res, err));
};

exports.update = (req, res) => {
  let user = new Staff(req.body);
  user.password = user.setPassword(req.body.password);
  user = user.toObject();
  if (user._id) delete user._id;
  if (user.id) delete user.id;
  Staff.findOneAndUpdateAsync({_id: req.params.id}, {$set: user}, {new: true})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleError(res, err))
};

exports.delete = (req, res) => {
  Staff.findOneAndUpdateAsync({_id: req.params.id}, {$set: {isDeleted: true}}, {new: true})
      .then(member => {
        Business.findOneAndUpdateAsync({_id: member.company}, {$pull: {'staff': member._id}})
            .then(() => {
              if (member.role === 'manager' && member.offices.length > 0) {
                async.each(member.offices, (office, callback) => {
                  Office.findOneAndUpdateAsync({_id: office}, {$pull: {'managers': member._id}})
                      .then(() => {
                        callback();
                      })
                      .catch(err => {
                        return helper.handleError(res, err);
                      })
                }, (err) => {
                  if (err) return helper.handleError(err);
                });
              }
              if (member.role === 'agent' && member.offices.length > 0) {
                async.each(member.offices, (office, callback) => {
                  Office.findOneAndUpdateAsync({_id: office}, {$pull: {'agents': member._id}})
                      .then(() => {
                        callback();
                      })
                      .catch(err => {
                        return helper.handleError(res, err);
                      })
                }, (err) => {
                  if (err) return helper.handleError(err);
                });
              }
              helper.handleResponse(res, member);
            })
            .catch(err => helper.handleError(res, err));
      })
      .catch(err => helper.handleError(res, err))
};

exports.search = (req, res) => {
  Staff.findAsync({
    isDeleted: false,
    $or: [
      {firstName: {$regex: req.params.query, $options: 'i'}},
      {lastName: {$regex: req.params.query, $options: 'i'}}
    ]
  })
      .then(docs => helper.handleResponse(res, docs))
      .catch(err => helper.handleError(res, err))
};

exports.assignCompanyToMember = (req, res) => {
  let memberRole = req.body.memberRole;
  let companyId = req.body.companyId;
  Business.findOneAsync({_id: companyId})
      .then(doc => {
        Business.populate(doc, {path: 'staff', model: 'Staff'}, (err, company) => {
          let staff = company.staff;
          if (memberRole === 'admin') {
            let admin = _.find(staff, member => member.role === memberRole);
            _.isUndefined(admin) || _.isEmpty(admin) || _.isNull(admin)
                ? res.status(200).json({message: 'okay'})
                : res.status(422).json({message: 'This company already has an Admin, you can not assign multiple admins for the same company'});
          } else {
            let members = _.filter(company.staff, member => member.role === memberRole);
            switch (memberRole) {
              case 'manager':
                members.length >= company.manager
                    ? res.status(422).json({message: `Quota for company managers is exceeded, you can not add more managers`})
                    : res.status(200).json({message: 'okay'});
                break;
              case 'agent':
                members.length >= company.agent
                    ? res.status(422).json({message: `Quota for company agents is exceeded, you can not add more agents`})
                    : res.status(200).json({message: 'okay'});
                break;
            }
          }
        });
      })
      .catch(err => {
        return res.status(500).json({message: err.message})
      })
};

exports.searchByCompany = (req, res) => {
  Staff.findAsync({
    isDeleted: false,
    company: req.body.company,
    role: req.body.role,
    $or: [
      {firstName: {$regex: req.body.name, $options: 'i'}},
      {lastName: {$regex: req.body.name, $options: 'i'}}
    ]
  })
      .then(docs => helper.handleResponse(res, docs))
      .catch(err => helper.handleError(res, err))
};

exports.removeOfficeFromMembers = (req, res) => {
  Staff.findOneAndUpdateAsync({_id: req.body.personId}, {$pull: {'offices': req.body.officeId}})
      .then((doc) => {
        if (doc.role === 'manager') {
          Office.updateAsync({_id: req.body.officeId}, {$pull: {'managers': doc._id}})
              .then(() => helper.handleResponse(res, doc))
              .catch(err => helper.handleError(res, err))
        }
        if (doc.role === 'agent') {
          Office.updateAsync({_id: req.body.officeId}, {$pull: {'agents': doc._id}})
              .then(() => helper.handleResponse(res, doc))
              .catch(err => helper.handleError(res, err))
        }
      })
      .catch(err => helper.handleError(res, err));
};

exports.addOfficeToMembers = (req, res) => {
  if (req.body._id) delete req.body._id;
  if (req.body.id) delete req.body.id;
  Staff.findOneAndUpdateAsync({_id: req.body.personId}, {$push: {'offices': req.body.officeId}}, {new: true})
      .then((doc) => {
        if (doc.role === 'manager') {
          Office.findOneAndUpdateAsync({_id: req.body.officeId}, {$push: {'managers': doc._id}}, {new: true})
              .then((newDoc) => helper.handleResponse(res, doc))
              .catch(err => helper.handleError(res, err))
        }
        if (doc.role === 'agent') {
          Office.findOneAndUpdateAsync({_id: req.body.officeId}, {$push: {'agents': doc._id}}, {new: true})
              .then((newDoc) => helper.handleResponse(res, doc))
              .catch(err => helper.handleError(res, err))
        }
      })
      .catch(err => helper.handleError(res, err));
};