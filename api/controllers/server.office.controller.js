const Office = require('../models/server.office.model'),
    Business = require('../models/server.business.model'),
    Staff = require('../models/server.staff.model'),
    async = require('async'),
    helper = require('../helper');

exports.getAll = (req, res) => {
  let filters = {};
  filters.isDeleted = false;
  if (!req.body.offset) req.body.offset = 0;
  if (!req.body.limit) req.body.limit = 10;
  if (req.body.company) filters.company = req.body.company;
  if (req.decoded.role === 'manager' || req.decoded.role === 'agent') {
    Staff.findOneAsync({_id: req.decoded.userId})
        .then(user => {
          Staff.populate(user, {path: 'offices', model: 'Office'}, (err, populatedUser) => {
            return helper.handleResponse(res, {data: populatedUser.offices, totalItems: populatedUser.offices.length});
          })
        })
        .catch(err => helper.handleError(res, err))
  } else {
    Office.paginate(filters, {
      select: {officeName: 1, _id: 1, uniqueId: 1},
      offset: parseInt(req.body.offset),
      limit: parseInt(req.body.limit)
    })
        .then(result => helper.handleResponse(res, {data: result.docs, totalItems: result.total}))
        .catch(err => helper.handleError(res, err));
  }
};

exports.create = (req, res) => {
  req.body.createdBy = req.decoded.userId;
  Business.findOneAsync({_id: req.body.company})
      .then(doc => {
        Business.populate(doc, {path: 'offices', model: 'Office'}, (err, business) => {
          if (business.offices.length < business.office) {
            let office = new Office(req.body);
            Office.createAsync(office)
                .then(office => {
                  Business.findOneAndUpdateAsync({_id: office.company}, {$push: {'offices': office._id}})
                      .then(() => {
                        let members = office.managers.concat(office.agents);
                        if (members.length > 0) {
                          async.each(members, (member, callback) => {
                            Staff.findOneAndUpdateAsync({_id: member}, {$push: {'offices': office._id}})
                                .then(() => {
                                  callback();
                                })
                                .catch(err => {
                                  return helper.handleError(res, err);
                                })
                          }, (err) => {
                            if (err) return helper.handleError(err);
                          });
                        }
                        return helper.handleResponse(res, office);
                      })
                      .catch(err => helper.handleError(res, err));
                })
                .catch(err => helper.handleError(res, err))
          } else {
            return res.status(422).json({message: `Quota for number of offices is exceeded, you can not add more offices`})
          }
        })
      })
      .catch(err => helper.handleError(res, err));
};

exports.get = (req, res) => {
  Office.findOneAsync({_id: req.params.id})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleError(res, err))
};

exports.delete = (req, res) => {
  Office.findOneAndUpdateAsync({_id: req.params.id}, {$set: {isDeleted: true}}, {new: true})
      .then(office => {
        Business.findOneAndUpdateAsync({_id: office.company}, {$pull: {'offices': office._id}})
            .then(() => {
              let members = office.managers.concat(office.agents);
              if (members.length > 0) {
                async.each(members, (member, callback) => {
                  Staff.findOneAndUpdateAsync({_id: member}, {$pull: {'offices': office._id}})
                      .then(() => {
                        callback();
                      })
                      .catch(err => {
                        return helper.handleError(res, err);
                      })
                }, (err) => {
                  if (err) return helper.handleError(err);
                });
              }
              helper.handleResponse(res, office);
            })
            .catch(err => helper.handleError(res, err));
      })
      .catch(err => helper.handleResponse(res, err))
};

exports.update = (req, res) => {
  let office = new Office(req.body);
  office = office.toObject();
  if (office._id) delete office._id;
  if (office.id) delete office.id;
  Office.findOneAndUpdateAsync({_id: req.params.id}, {$set: office}, {new: true})
      .then(office => helper.handleResponse(res, office))
      .catch(err => helper.handleError(res, err))
};

exports.getOfficesByBusinessId = (req, res) => {
  let filters = {};
  filters.isDeleted = false;
  filters.company = req.params.companyId;
  if(req.decoded.role === 'manager') filters.managers = req.decoded.userId;
  Office.findAsync(filters)
      .then(docs => helper.handleResponse(res, docs))
      .catch(err => helper.handleError(res, err))
};

exports.searchOfficeByCompany = (req, res) => {
  let filters = {};
  filters.isDeleted = false;
  filters.company = req.params.company;
  filters.officeName = {$regex: req.params.name, $options: 'i'};
  if(req.decoded.role === 'manager') filters.managers = req.decoded.userId;
  Office.findAsync(filters)
      .then(docs => helper.handleResponse(res, docs))
      .catch(err => helper.handleError(res, err))
};

exports.removeStaffFromOffice = (req, res) => {
  Office.findOneAsync({_id: req.body.officeId})
      .then(singleOffice => {
        Staff.findOneAndUpdateAsync({_id: req.body.personId}, {$pull: {'offices': req.body.officeId}})
            .then(() => {
              if (req.body.role === 'manager') {
                Office.updateAsync({_id: singleOffice._id}, {$pull: {'managers': req.body.personId}})
                    .then(doc => helper.handleResponse(res, singleOffice))
                    .catch(err => helper.handleError(res, err))
              }

              if (req.body.role === 'agent') {
                Office.updateAsync({_id: singleOffice._id}, {$pull: {'agents': req.body.personId}})
                    .then(doc => helper.handleResponse(res, singleOffice))
                    .catch(err => helper.handleError(res, err))
              }
            })
            .catch(err => helper.handleError(res, err));
      })
      .catch(err => helper.handleError(res, err))
};

exports.addStaffToOffice = (req, res) => {
  Office.findOneAsync({_id: req.body.officeId})
      .then(singleOffice => {
        Staff.findOneAndUpdateAsync({_id: req.body.personId}, {$push: {'offices': req.body.officeId}})
            .then(() => {
              if (req.body.role === 'manager') {
                Office.updateAsync({_id: singleOffice._id}, {$push: {'managers': req.body.personId}})
                    .then(doc => helper.handleResponse(res, singleOffice))
                    .catch(err => helper.handleError(res, err))
              }

              if (req.body.role === 'agent') {
                Office.updateAsync({_id: singleOffice._id}, {$push: {'agents': req.body.personId}})
                    .then(doc => helper.handleResponse(res, singleOffice))
                    .catch(err => helper.handleError(res, err))
              }
            })
            .catch(err => helper.handleError(res, err));
      })
      .catch(err => helper.handleError(res, err))
};