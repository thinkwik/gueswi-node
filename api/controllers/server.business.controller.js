const Business = require('../models/server.business.model'),
    helper = require('../helper'),
    moment = require('moment');

exports.create = (req, res) => {
  let business = new Business(req.body);
  business.password = business.setPassword(req.body.password);
  business.contractStartDate = moment(req.body.contractStartDate).startOf('day');
  business.contractEndDate = moment(req.body.contractEndDate).endOf('day');
  business.createdBy = req.decoded.userId;
  Business.findOneAsync({email: req.body.email})
      .then(doc => {
        if (doc === null) {
          Business.createAsync(business)
              .then(doc => helper.handleResponse(res, doc))
              .catch(err => helper.handleError(res, err))
        } else {
          return res.status(422).json({message: 'One business is already registered with this email!'});
        }
      })
      .catch(err => helper.handleError(res, err));

};

exports.get = (req, res) => {
  Business.findOneAsync({_id: req.params.id})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleError(res, err))
};

exports.getAll = (req, res) => {
  if (!req.body.offset) req.body.offset = 0;
  if (!req.body.limit) req.body.limit = 10;
  Business.paginate({isDeleted: false}, {
    select: {name: 1, _id: 1, contractEndDate: 1, contractStartDate: 1},
    offset: parseInt(req.body.offset),
    limit: parseInt(req.body.limit)
  })
      .then(result => helper.handleResponse(res, {data: result.docs, totalItems: result.total}))
      .catch(err => helper.handleError(res, err));
};

exports.update = (req, res) => {
  let business = new Business(req.body);
  if (req.body.password) business.password = business.setPassword(req.body.password);
  if (req.body.contractStartDate) business.contractStartDate = moment(req.body.contractStartDate).startOf('day');
  if (req.body.contractEndDate) business.contractEndDate = moment(req.body.contractEndDate).endOf('day');
  business = business.toObject();
  if (business._id) delete business._id;
  Business.findOneAndUpdateAsync({_id: req.params.id}, {$set: business}, {new: true})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleResponse(res, err))
};

exports.delete = (req, res) => {
  Business.findOneAndUpdateAsync({_id: req.params.id}, {$set: {isDeleted: true}}, {new: true})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleError(res, err))
};

exports.search = (req, res) => {
  Business.findAsync({name: {$regex: req.params.query, $options: 'i'}, isDeleted: false})
      .then(docs => helper.handleResponse(res, docs))
      .catch(err => helper.handleError(res, err))
};

exports.updateBusinessForAdmin = (req, res) => {
  Business.findOneAndUpdateAsync(
      {_id: req.params.id},
      {
        $set: {
          name: req.body.name,
          tradeName: req.body.tradeName,
          location: req.body.location,
          phone: req.body.phone,
          nameOfRoad: req.body.nameOfRoad,
          CIF: req.body.CIF,
          numberOfWay: req.body.numberOfWay,
          contactPerson: req.body.contactPerson,
          image: req.body.image
        },
      },
      {new: true})
      .then(doc => helper.handleResponse(res, doc))
      .catch(err => helper.handleResponse(res, err))
};
